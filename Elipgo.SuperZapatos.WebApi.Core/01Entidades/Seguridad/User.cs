﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad._Base;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad
{
   [Table("Users", Schema = "Security")]
   public class User : BaseSeguridad, IUser
   {
      #region   < < <   C o n s t r u c t o r e s   > > >
      public User()
      {
         
      }
      #endregion

      #region   < < <   P r o p i e d a d e s   n a t u r a l e s   > > >
      [Required(ErrorMessage = "La columna {0} es requerida.")]
      [StringLength(127)]
      [Column(TypeName = "nvarchar(127)")]
      [Display(Name = "Contraseña")]
      public string Password { get; set; }

      [Required(ErrorMessage = "La columna {0} es requerida.")]
      [StringLength(100)]
      [Column(TypeName = "nvarchar(100)")]
      [Display(Name = "Nombre")]
      public string NameUser { get; set; }

      [Required(ErrorMessage = "La columna {0} es requerida.")]
      [StringLength(100)]
      [Column(TypeName = "nvarchar(100)")]
      [Display(Name = "Apellido paterno")]
      public string LastNameUser { get; set; }

      [Required(ErrorMessage = "La columna {0} es requerida.")]
      [StringLength(100)]
      [Column(TypeName = "nvarchar(100)")]
      [Display(Name = "Apellido materno")]
      public string MothersLastNameUser { get; set; }

      [Required(ErrorMessage = "La columna {0} es requerida.")]
      [StringLength(320)]
      [Column(TypeName = "nvarchar(320)")]
      [Display(Name = "Correo electrónico")]
      public string EmailUser { get; set; }
      #endregion

      #region   < < <   D e p e n d e n c i a s   > > >
      [Required]
      [Column(TypeName = "nvarchar(450)")]
      [Display(Name = "Rol")]
      public string RoleId { get; set; }
      #endregion

      #region   < < <   P r o p i e d a d e s   d e   n a v e g a c i ó n   > > >
      #region   < < <   P a d r e s   > > >
      [ForeignKey("RoleId")]
      [InverseProperty("Users")]
      [Display(Name = "Rol")]
      public Role Role { get; set; }
      #endregion

      #region   < < <   H i j o s   > > >
      
      #endregion
      #endregion
   }
}
