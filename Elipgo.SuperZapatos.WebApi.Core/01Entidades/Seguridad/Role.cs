﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad._Base;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad
{
   [Table("Roles", Schema = "Security")]
   public class Role : BaseSeguridad, IRole
   {
      #region   < < <   C o n s t r u c t o r e s   > > >
      public Role()
      {
         Users = new HashSet<User>();
      }
      #endregion

      #region   < < <   P r o p i e d a d e s   n a t u r a l e s   > > >
      [Required(ErrorMessage = "La columna {0} es requerida.")]
      [StringLength(200)]
      [Column(TypeName = "nvarchar(200)")]
      [Display(Name = "Descripción")]
      public string Description { get; set; }
      #endregion

      #region   < < <   P r o p i e d a d e s   d e   n a v e g a c i ó n   > > >
      #region   < < <   P a d r e s   > > >
      #endregion

      #region   < < <   H i j o s   > > >
      [InverseProperty("Role")]
      public ICollection<User> Users { get; set; }
      #endregion
      #endregion
   }
}
