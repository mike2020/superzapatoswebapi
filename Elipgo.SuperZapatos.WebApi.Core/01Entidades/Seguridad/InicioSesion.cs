﻿namespace Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad
{
   public class InicioSesion
   {
      public string Usuario { get; set; }
      public string Contrasenia { get; set; }
   }
}
