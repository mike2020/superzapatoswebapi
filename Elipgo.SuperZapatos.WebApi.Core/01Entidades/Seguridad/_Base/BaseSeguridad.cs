﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad._Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad._Base
{
   public abstract class BaseSeguridad : BaseControlSeguridad, IBaseSeguridad
   {
      #region   < < <   L l a v e   p r i m a r i a   > > >
      [Key]
      [Column(TypeName = "nvarchar(450)")]
      [Display(Name = "Id")]
      public string Id { get; set; }

      [Required(ErrorMessage = "La columna {0} es requerida.")]
      [StringLength(320)]
      [Column(TypeName = "nvarchar(320)")]
      [Display(Name = "Usuario")]
      public string Name { get; set; }
      #endregion
   }
}
