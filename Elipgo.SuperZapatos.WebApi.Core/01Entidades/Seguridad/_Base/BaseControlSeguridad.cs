﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad._Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad._Base
{
   public abstract class BaseControlSeguridad : IBaseControlSeguridad
   {
      [Required]
      [Range(0, 1)]
      [Column(TypeName = "int")]
      [Display(Name = "Estatus")]
      public int Status { get; set; }

      [Required]
      [Column(TypeName = "datetime")]
      [Display(Name = "Fecha de creación")]
      public DateTime CreationDate { get; set; }
   }
}
