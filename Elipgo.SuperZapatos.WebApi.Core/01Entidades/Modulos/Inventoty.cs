﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades._Base;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Modulos;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos
{
   [Table("Inventories", Schema = "Modules")]
   public class Inventory : BaseEntidad, IInventory
   {
      #region   < < <   C o n s t r u c t o r e s   > > >
      public Inventory()
      {

      }
      #endregion

      #region   < < <   P r o p i e d a d e s   n a t u r a l e s   > > >
      [Required(ErrorMessage = "La columna {0} es requerida.")]
      [Column(TypeName = "int")]
      [Display(Name = "Total en Estante")]
      public int TotalInShelf { get; set; }

      [Required(ErrorMessage = "La columna {0} es requerida.")]
      [Column(TypeName = "int")]
      [Display(Name = "Total en Bodega")]
      public int TotalInVault { get; set; }
      #endregion

      #region   < < <   D e p e n d e n c i a s   > > >
      [Required]
      [Column(TypeName = "int")]
      [Display(Name = "Tienda")]
      public int StoreId { get; set; }


      [Required]
      [Column(TypeName = "int")]
      [Display(Name = "Articulo")]
      public int ArticleId { get; set; }
      #endregion

      #region   < < <   P r o p i e d a d e s   d e   n a v e g a c i ó n   > > >
      #region   < < <   P a d r e s   > > >
      [ForeignKey("StoreId")]
      [InverseProperty("Inventories")]
      [Display(Name = "Tienda")]
      public Store Store { get; set; }


      [ForeignKey("ArticleId")]
      [InverseProperty("Inventories")]
      [Display(Name = "Artículo")]
      public Article Article { get; set; }
      #endregion

      #region   < < <   H i j o s   > > >

      #endregion
      #endregion
   }
}
