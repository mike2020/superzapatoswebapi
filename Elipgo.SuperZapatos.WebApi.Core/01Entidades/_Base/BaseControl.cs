﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces._Base;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace Elipgo.SuperZapatos.WebApi.Core._01Entidades._Base
{
   public abstract class BaseControl : IBaseControl
   {
      [Required]
      [Range(0, 1)]
      [Column(TypeName = "int")]
      [Display(Name = "Estatus")]
      public int Status { get; set; }

      [Required]
      [Column(TypeName = "nvarchar(450)")]
      [Display(Name = "Usuario creación")]
      public string CreationBy { get; set; }

      [Required]
      [Column(TypeName = "datetime")]
      [Display(Name = "Fecha de creación")]
      public DateTime CreationDate { get; set; }

      [Column(TypeName = "nvarchar(450)")]
      [Display(Name = "Usuario modificación")]
      public string LastUpdateBy { get; set; }

      [Column(TypeName = "datetime")]
      [Display(Name = "Fecha modificación")]
      public DateTime? LastUpdateDate { get; set; }
   }
}
