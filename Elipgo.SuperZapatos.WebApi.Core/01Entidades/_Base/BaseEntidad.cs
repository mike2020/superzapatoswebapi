﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces._Base;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elipgo.SuperZapatos.WebApi.Core._01Entidades._Base
{
   public class BaseEntidad : BaseControl, IBaseEntidad
   {
      #region   < < <   L l a v e   p r i m a r i a   > > >
      [Key]
      [Column(TypeName = "int")]
      [Display(Name = "Id")]
      public int Id { get; set; }
      #endregion
   }
}
