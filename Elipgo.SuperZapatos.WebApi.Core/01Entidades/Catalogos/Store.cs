﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades._Base;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Catalogos;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos
{
   [Table("Stores", Schema = "Catalogs")]
   public class Store : BaseEntidad, IStore
   {
      #region   < < <   C o n s t r u c t o r e s   > > >
      public Store()
      {
         Prices = new HashSet<Price>();
         Inventories = new HashSet<Inventory>();
      }
      #endregion

      #region   < < <   P r o p i e d a d e s   n a t u r a l e s   > > >
      [Required(ErrorMessage = "La columna {0} es requerida.")]
      [StringLength(150)]
      [Column(TypeName = "nvarchar(150)")]
      [Display(Name = "Nombre")]
      public string Name { get; set; }

      [Required(ErrorMessage = "La columna {0} es requerida.")]
      [StringLength(200)]
      [Column(TypeName = "nvarchar(200)")]
      [Display(Name = "Dirección")]
      public string Direction { get; set; }
      #endregion

      #region   < < <   P r o p i e d a d e s   d e   n a v e g a c i ó n   > > >
      #region   < < <   P a d r e s   > > >
      #endregion

      #region   < < <   H i j o s   > > >
      [InverseProperty("Store")]
      public ICollection<Price> Prices { get; set; }

      [InverseProperty("Store")]
      public ICollection<Inventory> Inventories { get; set; }
      #endregion
      #endregion
   }
}
