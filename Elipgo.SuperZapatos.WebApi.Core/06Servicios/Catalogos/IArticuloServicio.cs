﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Catalogos
{
   public interface IArticuloServicio
   {
      ListaPaginado<Article> ObtenerArticulos(ArticuloFiltrosConsulta filtros);
      Task<Article> ObtenerArticulo(int id);
      Task AgregarArticulo(Article articulo);
      Task<bool> ActualizarArticulo(Article articulo);
      Task<bool> EliminarArticulo(int id);
   }
}