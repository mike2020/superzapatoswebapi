﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._05Excepciones;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using Elipgo.SuperZapatos.WebApi.Core._08Opciones;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Catalogos
{
   public class ArticuloServicio : IArticuloServicio
   {
      private readonly IUnidadTrabajo _unidadTrabajo;
      private readonly IUnidadTrabajoSeguridad _unidadTrabajoSeguridad;
      private readonly PaginacionOpciones _paginacionOpciones;

      public ArticuloServicio(
                              IUnidadTrabajo unidadTrabajo,
                              IUnidadTrabajoSeguridad unidadTrabajoSeguridad,
                              IOptions<PaginacionOpciones> paginacionOpciones
                             )
      {
         _unidadTrabajo = unidadTrabajo;
         _unidadTrabajoSeguridad = unidadTrabajoSeguridad;
         _paginacionOpciones = paginacionOpciones.Value;
      }

      public ListaPaginado<Article> ObtenerArticulos(ArticuloFiltrosConsulta filtros)
      {
         var articulos = _unidadTrabajo.ArticuloRepositorio.ObtenerTodos();

         articulos = FiltrarArticulos(articulos, filtros);

         var paginaArticulos = ListaPaginado<Article>.Obtener(articulos, filtros.NumeroPagina, filtros.TamanioPagina);

         return paginaArticulos;
      }

      private IEnumerable<Article> FiltrarArticulos(IEnumerable<Article> articulos, ArticuloFiltrosConsulta filtros) {
         /*---   Se quitará de aqui esta inicialización   ---*/
         filtros.NumeroPagina = filtros.NumeroPagina == 0 ? _paginacionOpciones.DefaultPageNumber : filtros.NumeroPagina;
         filtros.TamanioPagina = filtros.TamanioPagina == 0 ? _paginacionOpciones.DefaultPageSize : filtros.TamanioPagina;

         if (filtros.Id != null)
         {
            articulos = articulos.Where(a => a.Id == filtros.Id);
         };

         if (filtros.FechaRegistro != null)
         {
            articulos = articulos.Where(a => Convert.ToDateTime(a.CreationDate).ToShortDateString() == Convert.ToDateTime(filtros.FechaRegistro).ToShortDateString());
         };

         if (filtros.Nombre != null)
         {
            articulos = articulos.Where(a => a.Name.ToUpper().Contains(filtros.Nombre.ToUpper()));
         };

         return articulos;
      }

      public async Task<Article> ObtenerArticulo(int id)
      {
         var articulo = await _unidadTrabajo.ArticuloRepositorio.ObtenerPorId(id);

         if (articulo == null) {
            throw new ExcepcionNegocio("El artículo no existe");
         }

         return articulo;
      }

      public async Task AgregarArticulo(Article articulo)
      {
         if(articulo.CreationBy == null || articulo.CreationBy.Trim() == String.Empty) {
            throw new ExcepcionNegocio("Debe proporcionar el usuario");
         };

         var usuario = await _unidadTrabajoSeguridad.UsuarioBaseRepositorioSeguridad.ObtenerPorId(articulo.CreationBy);

         if (usuario == null)
         {
            throw new ExcepcionNegocio("El usuario no existe");
         };

         await _unidadTrabajo.ArticuloRepositorio.Agregar(articulo);
         await _unidadTrabajo.SaveChangesAsync();
      }

      public async Task<bool> ActualizarArticulo(Article articulo)
      {
         if (articulo.LastUpdateBy == null || articulo.LastUpdateBy.Trim() == String.Empty)
         {
            throw new ExcepcionNegocio("Debe proporcionar el usuario");
         };

         var usuario = await _unidadTrabajoSeguridad.UsuarioBaseRepositorioSeguridad.ObtenerPorId(articulo.LastUpdateBy);

         if (usuario == null)
         {
            throw new ExcepcionNegocio("El usuario no existe");
         };

         _unidadTrabajo.ArticuloRepositorio.Actualizar(articulo);
         await _unidadTrabajo.SaveChangesAsync();
         return true;
      }
    
      public async Task<bool> EliminarArticulo(int id)
      {
         if (id == 0) {
            throw new ExcepcionNegocio("Debe proporcionar el id del artículo");
         };

         var articulo = await _unidadTrabajo.ArticuloRepositorio.ObtenerPorId(id);

         if (articulo == null) {
            throw new ExcepcionNegocio("El articulo no existe");
         }

         await _unidadTrabajo.ArticuloRepositorio.Eliminar(id);
         await _unidadTrabajo.SaveChangesAsync();
         return true;
      }
   }
}
