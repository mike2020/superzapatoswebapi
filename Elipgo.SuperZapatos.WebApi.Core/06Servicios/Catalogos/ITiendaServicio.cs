﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Catalogos
{
   public interface ITiendaServicio
   {
      ListaPaginado<Store> ObtenerTiendas(TiendaFiltrosConsulta filtros);
      Task<Store> ObtenerTienda(int id);
      Task AgregarTienda(Store tienda);
      Task<bool> ActualizarTienda(Store tienda);
      Task<bool> EliminarTienda(int id);
   }
}
