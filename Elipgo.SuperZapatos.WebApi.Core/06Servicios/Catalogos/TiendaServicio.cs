﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._05Excepciones;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using Elipgo.SuperZapatos.WebApi.Core._08Opciones;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Catalogos
{
   public class TiendaServicio : ITiendaServicio
   {
      private readonly IUnidadTrabajo _unidadTrabajo;
      private readonly IUnidadTrabajoSeguridad _unidadTrabajoSeguridad;
      private readonly PaginacionOpciones _paginacionOpciones;

      public TiendaServicio(
                            IUnidadTrabajo unidadTrabajo,
                            IUnidadTrabajoSeguridad unidadTrabajoSeguridad,
                            IOptions<PaginacionOpciones> paginacionOpciones
                           )
      {
         _unidadTrabajo = unidadTrabajo;
         _unidadTrabajoSeguridad = unidadTrabajoSeguridad;
         _paginacionOpciones = paginacionOpciones.Value;
      }

      public ListaPaginado<Store> ObtenerTiendas(TiendaFiltrosConsulta filtros)
      {
         var tiendas = _unidadTrabajo.TiendaRepositorio.ObtenerTodos();

         tiendas = FiltrarTiendas(tiendas, filtros);

         var paginaTiendas = ListaPaginado<Store>.Obtener(tiendas, filtros.NumeroPagina, filtros.TamanioPagina);

         return paginaTiendas;
      }

      private IEnumerable<Store> FiltrarTiendas(IEnumerable<Store> tiendas, TiendaFiltrosConsulta filtros)
      {
         /*---   Se quitará de aqui esta inicialización   ---*/
         filtros.NumeroPagina = filtros.NumeroPagina == 0 ? _paginacionOpciones.DefaultPageNumber : filtros.NumeroPagina;
         filtros.TamanioPagina = filtros.TamanioPagina == 0 ? _paginacionOpciones.DefaultPageSize : filtros.TamanioPagina;

         if (filtros.Id != null)
         {
            tiendas = tiendas.Where(a => a.Id == filtros.Id);
         };

         if (filtros.FechaRegistro != null)
         {
            tiendas = tiendas.Where(a => Convert.ToDateTime(a.CreationDate).ToShortDateString() == Convert.ToDateTime(filtros.FechaRegistro).ToShortDateString());
         };

         if (filtros.Nombre != null)
         {
            tiendas = tiendas.Where(a => a.Name.ToUpper().Contains(filtros.Nombre.ToUpper()));
         };

         return tiendas;
      }

      public async Task<Store> ObtenerTienda(int id)
      {
         var tienda = await _unidadTrabajo.TiendaRepositorio.ObtenerPorId(id);

         if (tienda == null) {
            throw new ExcepcionNegocio("La tienda no existe");
         }

         return tienda;
      }

      public async Task AgregarTienda(Store tienda)
      {
         if (tienda.CreationBy == null || tienda.CreationBy.Trim() == String.Empty)
         {
            throw new ExcepcionNegocio("Debe proporcionar el usuario");
         };

         var usuario = await _unidadTrabajoSeguridad.UsuarioBaseRepositorioSeguridad.ObtenerPorId(tienda.CreationBy);

         if (usuario == null)
         {
            throw new ExcepcionNegocio("El usuario no existe");
         };

         await _unidadTrabajo.TiendaRepositorio.Agregar(tienda);
         await _unidadTrabajo.SaveChangesAsync();
      }

      public async Task<bool> ActualizarTienda(Store tienda)
      {
         if (tienda.LastUpdateBy == null || tienda.LastUpdateBy.Trim() == String.Empty)
         {
            throw new ExcepcionNegocio("Debe proporcionar el usuario");
         };

         var usuario = await _unidadTrabajoSeguridad.UsuarioBaseRepositorioSeguridad.ObtenerPorId(tienda.LastUpdateBy);

         if (usuario == null)
         {
            throw new ExcepcionNegocio("El usuario no existe");
         };

         _unidadTrabajo.TiendaRepositorio.Actualizar(tienda);
         await _unidadTrabajo.SaveChangesAsync();
         return true;
      }

      public async Task<bool> EliminarTienda(int id)
      {
         if (id == 0)
         {
            throw new ExcepcionNegocio("Debe proporcionar el id de la tienda");
         };

         var tienda = await _unidadTrabajo.TiendaRepositorio.ObtenerPorId(id);

         if (tienda == null)
         {
            throw new ExcepcionNegocio("La tienda no existe");
         }

         await _unidadTrabajo.TiendaRepositorio.Eliminar(id);
         await _unidadTrabajo.SaveChangesAsync();
         return true;
      }
   }
}
