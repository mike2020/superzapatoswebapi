﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._05Excepciones;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using Elipgo.SuperZapatos.WebApi.Core._08Opciones;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Modulos
{
   public class PrecioServicio : IPrecioServicio
   {
      private readonly IUnidadTrabajo _unidadTrabajo;
      private readonly IUnidadTrabajoSeguridad _unidadTrabajoSeguridad;
      private readonly PaginacionOpciones _paginacionOpciones;

      public PrecioServicio(
                            IUnidadTrabajo unidadTrabajo,
                            IUnidadTrabajoSeguridad unidadTrabajoSeguridad,
                            IOptions<PaginacionOpciones> paginacionOpciones
                           )
      {
         _unidadTrabajo = unidadTrabajo;
         _unidadTrabajoSeguridad = unidadTrabajoSeguridad;
         _paginacionOpciones = paginacionOpciones.Value;
      }

      public ListaPaginado<Price> ObtenerPrecios(PrecioFiltrosConsulta filtros)
      {
         var precios = _unidadTrabajo.PrecioRepositorio.ObtenerTodos();

         precios = FiltrarPrecios(precios, filtros);

         var paginaPrecios = ListaPaginado<Price>.Obtener(precios, filtros.NumeroPagina, filtros.TamanioPagina);

         return paginaPrecios;
      }

      private IEnumerable<Price> FiltrarPrecios(IEnumerable<Price> precios, PrecioFiltrosConsulta filtros)
      {
         /*---   Se quitará de aqui esta inicialización   ---*/
         filtros.NumeroPagina = filtros.NumeroPagina == 0 ? _paginacionOpciones.DefaultPageNumber : filtros.NumeroPagina;
         filtros.TamanioPagina = filtros.TamanioPagina == 0 ? _paginacionOpciones.DefaultPageSize : filtros.TamanioPagina;

         if (filtros.FechaRegistro != null)
         {
            precios = precios.Where(a => Convert.ToDateTime(a.CreationDate).ToShortDateString() == Convert.ToDateTime(filtros.FechaRegistro).ToShortDateString());
         };

         if (filtros.Tienda != null)
         {
            precios = precios.Where(a => a.Store.Name.ToUpper().Contains(filtros.Tienda.ToUpper()));
         };

         if (filtros.Articulo != null)
         {
            precios = precios.Where(a => a.Article.Name.ToUpper().Contains(filtros.Articulo.ToUpper()));
         };

         return precios;
      }


      public async Task<Price> ObtenerPrecio(int id)
      {
         var precio = await _unidadTrabajo.PrecioRepositorio.ObtenerPorId(id);

         if (precio == null) {
            throw new ExcepcionNegocio("El precio no existe");
         }

         return precio;
      }

      public async Task AgregarPrecio(Price precio)
      {
         if (precio.CreationBy == null || precio.CreationBy.Trim() == String.Empty)
         {
            throw new ExcepcionNegocio("Debe proporcionar el usuario");
         };

         var usuario = await _unidadTrabajoSeguridad.UsuarioBaseRepositorioSeguridad.ObtenerPorId(precio.CreationBy);

         if (usuario == null)
         {
            throw new ExcepcionNegocio("El usuario no existe");
         };

         await _unidadTrabajo.PrecioRepositorio.Agregar(precio);
         await _unidadTrabajo.SaveChangesAsync();
      }

      public async Task<bool> ActualizarPrecio(Price precio)
      {
         if (precio.LastUpdateBy == null || precio.LastUpdateBy.Trim() == String.Empty)
         {
            throw new ExcepcionNegocio("Debe proporcionar el usuario");
         };

         var usuario = await _unidadTrabajoSeguridad.UsuarioBaseRepositorioSeguridad.ObtenerPorId(precio.LastUpdateBy);

         if (usuario == null)
         {
            throw new ExcepcionNegocio("El usuario no existe");
         };

         _unidadTrabajo.PrecioRepositorio.Actualizar(precio);
         await _unidadTrabajo.SaveChangesAsync();
         return true;
      }

      public async Task<bool> EliminarPrecio(int id)
      {
         if (id == 0)
         {
            throw new ExcepcionNegocio("Debe proporcionar el id del precio");
         };

         var articulo = await _unidadTrabajo.PrecioRepositorio.ObtenerPorId(id);

         if (articulo == null)
         {
            throw new ExcepcionNegocio("El precio no existe");
         }

         await _unidadTrabajo.PrecioRepositorio.Eliminar(id);
         await _unidadTrabajo.SaveChangesAsync();
         return true;
      }
   }
}
