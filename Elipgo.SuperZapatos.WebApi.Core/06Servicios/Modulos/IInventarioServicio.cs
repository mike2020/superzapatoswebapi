﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Modulos
{
   public interface IInventarioServicio
   {
      ListaPaginado<Inventory> ObtenerInventarios(InventarioFiltrosConsulta filtros);
      Task<Inventory> ObtenerInventario(int id);
      Task AgregarInventario(Inventory inventario);
      Task<bool> ActualizarInventario(Inventory inventario);
      Task<bool> EliminarInventario(int id);
   }
}
