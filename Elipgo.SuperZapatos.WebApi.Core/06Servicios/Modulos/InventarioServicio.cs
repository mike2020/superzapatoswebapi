﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._05Excepciones;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using Elipgo.SuperZapatos.WebApi.Core._08Opciones;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Modulos
{
   public class InventarioServicio : IInventarioServicio
   {
      private readonly IUnidadTrabajo _unidadTrabajo;
      private readonly IUnidadTrabajoSeguridad _unidadTrabajoSeguridad;
      private readonly PaginacionOpciones _paginacionOpciones;

      public InventarioServicio(
                                IUnidadTrabajo unidadTrabajo,
                                IUnidadTrabajoSeguridad unidadTrabajoSeguridad,
                                IOptions<PaginacionOpciones> paginacionOpciones
                               )
      {
         _unidadTrabajo = unidadTrabajo;
         _unidadTrabajoSeguridad = unidadTrabajoSeguridad;
         _paginacionOpciones = paginacionOpciones.Value;
      }

      public ListaPaginado<Inventory> ObtenerInventarios(InventarioFiltrosConsulta filtros)
      {
         var inventarios =  _unidadTrabajo.InventarioRepositorio.ObtenerTodos();

         inventarios = FiltrarInventarios(inventarios, filtros);

         var paginaInventarios = ListaPaginado<Inventory>.Obtener(inventarios, filtros.NumeroPagina, filtros.TamanioPagina);

         return paginaInventarios;
      }

      private IEnumerable<Inventory> FiltrarInventarios(IEnumerable<Inventory> inventarios, InventarioFiltrosConsulta filtros)
      {
         /*---   Se quitará de aqui esta inicialización   ---*/
         filtros.NumeroPagina = filtros.NumeroPagina == 0 ? _paginacionOpciones.DefaultPageNumber : filtros.NumeroPagina;
         filtros.TamanioPagina = filtros.TamanioPagina == 0 ? _paginacionOpciones.DefaultPageSize : filtros.TamanioPagina;

         if (filtros.FechaRegistro != null)
         {
            inventarios = inventarios.Where(a => Convert.ToDateTime(a.CreationDate).ToShortDateString() == Convert.ToDateTime(filtros.FechaRegistro).ToShortDateString());
         };

         if (filtros.Tienda != null)
         {
            inventarios = inventarios.Where(a => a.Store.Name.ToUpper().Contains(filtros.Tienda.ToUpper()));
         };

         if (filtros.Articulo != null)
         {
            inventarios = inventarios.Where(a => a.Article.Name.ToUpper().Contains(filtros.Articulo.ToUpper()));
         };

         return inventarios;
      }


      public async Task<Inventory> ObtenerInventario(int id)
      {
         var inventario = await _unidadTrabajo.InventarioRepositorio.ObtenerPorId(id);

         if (inventario == null)
         {
            throw new ExcepcionNegocio("El inventario no existe");
         }

         return inventario;
      }

      public async Task AgregarInventario(Inventory inventario)
      {
         if (inventario.CreationBy == null || inventario.CreationBy.Trim() == String.Empty)
         {
            throw new ExcepcionNegocio("Debe proporcionar el usuario");
         };

         var usuario = await _unidadTrabajoSeguridad.UsuarioBaseRepositorioSeguridad.ObtenerPorId(inventario.CreationBy);

         if (usuario == null)
         {
            throw new ExcepcionNegocio("El usuario no existe");
         };

         await _unidadTrabajo.InventarioRepositorio.Agregar(inventario);
         await _unidadTrabajo.SaveChangesAsync();
      }

      public async Task<bool> ActualizarInventario(Inventory inventario)
      {
         if (inventario.LastUpdateBy == null || inventario.LastUpdateBy.Trim() == String.Empty)
         {
            throw new ExcepcionNegocio("Debe proporcionar el usuario");
         };

         var usuario = await _unidadTrabajoSeguridad.UsuarioBaseRepositorioSeguridad.ObtenerPorId(inventario.LastUpdateBy);

         if (usuario == null)
         {
            throw new ExcepcionNegocio("El usuario no existe");
         };

         _unidadTrabajo.InventarioRepositorio.Actualizar(inventario);
         await _unidadTrabajo.SaveChangesAsync();
         return true;
      }

      public async Task<bool> EliminarInventario(int id)
      {
         if (id == 0)
         {
            throw new ExcepcionNegocio("Debe proporcionar el id del inventario");
         };

         var inventario = await _unidadTrabajo.InventarioRepositorio.ObtenerPorId(id);

         if (inventario == null)
         {
            throw new ExcepcionNegocio("El inventario no existe");
         }

         await _unidadTrabajo.InventarioRepositorio.Eliminar(id);
         await _unidadTrabajo.SaveChangesAsync();
         return true;
      }
   }
}
