﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Modulos
{
   public interface IPrecioServicio
   {
      ListaPaginado<Price> ObtenerPrecios(PrecioFiltrosConsulta filtros);
      Task<Price> ObtenerPrecio(int id);
      Task AgregarPrecio(Price precio);
      Task<bool> ActualizarPrecio(Price precio);
      Task<bool> EliminarPrecio(int id);
   }
}
