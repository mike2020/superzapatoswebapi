﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._08Opciones.Seguridad;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Seguridad
{
   public class TokenServicio : ITokenServicio
   {
      private readonly ITokenRepositorioSeguridad _tokenRepositorio;
      private readonly AutenticacionOpciones _autenticacionOpciones;

      public TokenServicio(
                           ITokenRepositorioSeguridad tokenRepositorio,
                           IOptions<AutenticacionOpciones> autenticacionOpciones
                          )
      {
         _tokenRepositorio = tokenRepositorio;
         _autenticacionOpciones = autenticacionOpciones.Value;
      }

      public async Task<string> GenerarToken(InicioSesion inicioSesion)
      {
         return await _tokenRepositorio.GenerarToken(inicioSesion, _autenticacionOpciones);
      }
   }
}
