﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Seguridad
{
   public interface ITokenServicio
   {
      Task<string> GenerarToken(InicioSesion inicioSesion);
   }
}