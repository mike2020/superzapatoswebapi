﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Seguridad;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Seguridad
{
   public interface IRolApiServicio
   {
      Task<bool> Actualizar(RoleApi rolApi);
      Task Agregar(RoleApi rolApi);
      Task<bool> Eliminar(string id);
      Task<RoleApi> ObtenerPorId(string id);
      Task<ListaPaginado<RoleApi>> ObtenerTodos(RolApiFiltrosConsulta filtros);
   }
}