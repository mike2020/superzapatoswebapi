﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Seguridad;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Seguridad
{
   public interface IUsuarioApiServicio
   {
      Task<ListaPaginado<UserApi>> ObtenerTodos(UsuarioApiFiltrosConsulta filtros);
      Task<UserApi> ObtenerPorId(string id);
      Task Agregar(UserApi usuarioApi);
      Task<bool> Actualizar(UserApi usuarioApi);
      Task<bool> Eliminar(string id);
      Task<UserApi> ObtenerCredenciaes(InicioSesion inicioSesion);
      Task RegistrarUsuario(UserApi usuario);
   }
}