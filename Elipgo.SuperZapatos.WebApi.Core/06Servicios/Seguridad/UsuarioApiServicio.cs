﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._05Excepciones;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._08Opciones;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Seguridad
{
   public class UsuarioApiServicio : IUsuarioApiServicio
   {
      private readonly IUnidadTrabajoSeguridad _unidadTrabajoSeguridad;
      private readonly PaginacionOpciones _paginacionOpciones;

      public UsuarioApiServicio(
                                IUnidadTrabajoSeguridad unidadTrabajoSeguridad,
                                IOptions<PaginacionOpciones> paginacionOpciones
                               )
      {
         _unidadTrabajoSeguridad = unidadTrabajoSeguridad;
         _paginacionOpciones = paginacionOpciones.Value;
      }

      public async Task<ListaPaginado<UserApi>> ObtenerTodos(UsuarioApiFiltrosConsulta filtros)
      {
         var usuariosApi = await _unidadTrabajoSeguridad.UsuarioApiRepositorioSeguridad.ObtenerTodos();

         usuariosApi = Filtrar(usuariosApi, filtros);

         var paginaUsuariosApi = ListaPaginado<UserApi>.Obtener(usuariosApi, filtros.NumeroPagina, filtros.TamanioPagina);

         return paginaUsuariosApi;
      }

      private IEnumerable<UserApi> Filtrar(IEnumerable<UserApi> usuariosApi, UsuarioApiFiltrosConsulta filtros)
      {
         /*---   Se quitará de aqui esta inicialización   ---*/
         filtros.NumeroPagina = filtros.NumeroPagina == 0 ? _paginacionOpciones.DefaultPageNumber : filtros.NumeroPagina;
         filtros.TamanioPagina = filtros.TamanioPagina == 0 ? _paginacionOpciones.DefaultPageSize : filtros.TamanioPagina;

         if (filtros.Id != null)
         {
            usuariosApi = usuariosApi.Where(a => a.Id == filtros.Id);
         };

         if (filtros.FechaRegistro != null)
         {
            usuariosApi = usuariosApi.Where(a => Convert.ToDateTime(a.CreationDate).ToShortDateString() == Convert.ToDateTime(filtros.FechaRegistro).ToShortDateString());
         };

         if (filtros.Nombre != null)
         {
            usuariosApi = usuariosApi.Where(a => a.Name.ToUpper().Contains(filtros.Nombre.ToUpper()));
         };

         return usuariosApi;
      }

      public async Task<UserApi> ObtenerPorId(string id)
      {
         return await _unidadTrabajoSeguridad.UsuarioApiRepositorioSeguridad.ObtenerPorId(id);
      }

      public async Task Agregar(UserApi usuarioApi)
      {
         await _unidadTrabajoSeguridad.UsuarioApiRepositorioSeguridad.Agregar(usuarioApi);
         await _unidadTrabajoSeguridad.SaveChangesAsync();
      }

      public async Task<bool> Actualizar(UserApi usuarioApi)
      {
         var usuarioApiActual = await ObtenerPorId(usuarioApi.Id);

         usuarioApi.CreationDate = usuarioApiActual.CreationDate;

         await _unidadTrabajoSeguridad.UsuarioApiRepositorioSeguridad.Actualizar(usuarioApi);
         await _unidadTrabajoSeguridad.SaveChangesAsync();
         return true;
      }

      public async Task<bool> Eliminar(string id)
      {
         if (id == String.Empty || id.Trim() == String.Empty)
         {
            throw new ExcepcionNegocio("Debe proporcionar el id del artículo");
         };

         var usuarioApi = await _unidadTrabajoSeguridad.UsuarioApiRepositorioSeguridad.ObtenerPorId(id);

         if (usuarioApi == null)
         {
            throw new ExcepcionNegocio("El usuarioApi no existe");
         }

         await _unidadTrabajoSeguridad.UsuarioApiRepositorioSeguridad.Eliminar(id);
         await _unidadTrabajoSeguridad.SaveChangesAsync();
         return true;
      }

      public async Task<UserApi> ObtenerCredenciaes(InicioSesion inicioSesion)
      {
         return await _unidadTrabajoSeguridad.UsuarioApiRepositorioSeguridad.ObtenerCredenciales(inicioSesion);
      }

      public async Task RegistrarUsuario(UserApi usuario)
      {
         await _unidadTrabajoSeguridad.UsuarioApiRepositorioSeguridad.Agregar(usuario);
         await _unidadTrabajoSeguridad.SaveChangesAsync();
      }
   }
}
