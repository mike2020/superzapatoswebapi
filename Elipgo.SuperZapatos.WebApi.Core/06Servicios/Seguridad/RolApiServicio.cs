﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._05Excepciones;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._08Opciones;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._06Servicios.Seguridad
{
   public class RolApiServicio : IRolApiServicio
   {
      private readonly IUnidadTrabajoSeguridad _unidadTrabajoSeguridad;
      private readonly PaginacionOpciones _paginacionOpciones;

      public RolApiServicio(
                            IUnidadTrabajoSeguridad unidadTrabajoSeguridad,
                            IOptions<PaginacionOpciones> paginacionOpciones
                           )
      {
         _unidadTrabajoSeguridad = unidadTrabajoSeguridad;
         _paginacionOpciones = paginacionOpciones.Value;
      }

      public async Task<ListaPaginado<RoleApi>> ObtenerTodos(RolApiFiltrosConsulta filtros)
      {
         var rolesApi = await _unidadTrabajoSeguridad.RolApiRepositorioSeguridad.ObtenerTodos();

         rolesApi = Filtrar(rolesApi, filtros);

         var paginaUsuariosApi = ListaPaginado<RoleApi>.Obtener(rolesApi, filtros.NumeroPagina, filtros.TamanioPagina);

         return paginaUsuariosApi;
      }

      private IEnumerable<RoleApi> Filtrar(IEnumerable<RoleApi> rolesApi, RolApiFiltrosConsulta filtros)
      {
         /*---   Se quitará de aqui esta inicialización   ---*/
         filtros.NumeroPagina = filtros.NumeroPagina == 0 ? _paginacionOpciones.DefaultPageNumber : filtros.NumeroPagina;
         filtros.TamanioPagina = filtros.TamanioPagina == 0 ? _paginacionOpciones.DefaultPageSize : filtros.TamanioPagina;

         if (filtros.Id != null)
         {
            rolesApi = rolesApi.Where(a => a.Id == filtros.Id);
         };

         if (filtros.FechaRegistro != null)
         {
            rolesApi = rolesApi.Where(a => Convert.ToDateTime(a.CreationDate).ToShortDateString() == Convert.ToDateTime(filtros.FechaRegistro).ToShortDateString());
         };

         if (filtros.Nombre != null)
         {
            rolesApi = rolesApi.Where(a => a.Name.ToUpper().Contains(filtros.Nombre.ToUpper()));
         };

         return rolesApi;
      }

      public async Task<RoleApi> ObtenerPorId(string id)
      {
         return await _unidadTrabajoSeguridad.RolApiRepositorioSeguridad.ObtenerPorId(id);
      }

      public async Task Agregar(RoleApi rolApi)
      {
         await _unidadTrabajoSeguridad.RolApiRepositorioSeguridad.Agregar(rolApi);
         await _unidadTrabajoSeguridad.SaveChangesAsync();
      }

      public async Task<bool> Actualizar(RoleApi rolApi)
      {
         var rolApiActual = await ObtenerPorId(rolApi.Id);

         rolApi.CreationDate = rolApiActual.CreationDate;

         await _unidadTrabajoSeguridad.RolApiRepositorioSeguridad.Actualizar(rolApi);
         await _unidadTrabajoSeguridad.SaveChangesAsync();
         return true;
      }

      public async Task<bool> Eliminar(string id)
      {
         if (id == String.Empty || id.Trim() == String.Empty)
         {
            throw new ExcepcionNegocio("Debe proporcionar el id");
         };

         var rolApi = await _unidadTrabajoSeguridad.RolApiRepositorioSeguridad.ObtenerPorId(id);

         if (rolApi == null)
         {
            throw new ExcepcionNegocio("El rolApi no existe");
         }

         await _unidadTrabajoSeguridad.RolApiRepositorioSeguridad.Eliminar(id);
         await _unidadTrabajoSeguridad.SaveChangesAsync();
         return true;
      }
   }
}
