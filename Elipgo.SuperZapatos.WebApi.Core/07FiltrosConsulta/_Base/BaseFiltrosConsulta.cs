﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos._Base
{
   public class BaseFiltrosConsulta
   {
      public int? Id { get; set; }
      public DateTime? FechaRegistro { get; set; }
      public int TamanioPagina { get; set; }
      public int NumeroPagina { get; set; }
   }
}
