﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Seguridad
{
   public class RolApiFiltrosConsulta
   {
      public string Id { get; set; }
      public DateTime? FechaRegistro { get; set; }
      public string Nombre { get; set; }
      public int TamanioPagina { get; set; }
      public int NumeroPagina { get; set; }
   }
}
