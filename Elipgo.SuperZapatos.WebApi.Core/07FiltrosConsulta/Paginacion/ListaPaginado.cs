﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion
{
   public class ListaPaginado<T> : List<T>
   {
      public ListaPaginado(List<T> renglones, int totalRenglones, int paginaNumero, int tamanioPagina)
      {
         TotalRenglones = totalRenglones;
         TamanioPagina = tamanioPagina;
         PaginaActual = paginaNumero;

         TotalPaginas = (int)Math.Ceiling(totalRenglones / (double)tamanioPagina);
         AddRange(renglones);
      }

      public int PaginaActual { get; set; }
      public int TotalPaginas { get; set; }
      public int TamanioPagina { get; set; }
      public int TotalRenglones { get; set; }
      public bool TienePaginaSiguiente => PaginaActual < TotalPaginas;
      public bool TienePaginaAnterior => PaginaActual > 1; 
      public int? PaginaSiguiente => TienePaginaSiguiente ? PaginaActual + 1 : (int?)null;
      public int? PaginaAnterior => TienePaginaAnterior ? PaginaActual - 1 : (int?)null;

      public static ListaPaginado<T> Obtener(IEnumerable<T> origen, int numeroPagina, int tamanioPagina)
      {
         var total = origen.Count();
         var renglones = origen.Skip((numeroPagina - 1) * tamanioPagina).Take(tamanioPagina).ToList();

         return new ListaPaginado<T>(renglones, total, numeroPagina, tamanioPagina);
      }
   }
}
