﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion
{
   public class Metadata
   {
      public int TotalRenglones { get; set; }
      public int TamanioPagina { get; set; }
      public int PaginaActual { get; set; }
      public int TotalPaginas { get; set; }
      public bool TienePaginaSiguiente { get; set; }
      public bool TienePaginaAnterior { get; set; }
      public string UrlPaginaSiguiente { get; set; }
      public string UrlPaginaAnterior { get; set; }
   }
}
