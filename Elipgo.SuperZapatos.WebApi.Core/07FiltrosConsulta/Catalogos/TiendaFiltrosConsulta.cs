﻿using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos._Base;

namespace Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Catalogos
{
   public class TiendaFiltrosConsulta : BaseFiltrosConsulta
   {
      public string Nombre { get; set; }
      public string Direction { get; set; }
   }
}
