﻿using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos._Base;

namespace Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Catalogos
{
   public class ArticuloFiltrosConsulta : BaseFiltrosConsulta
   {
      public string Nombre { get; set; }
      public string Description { get; set; }
   }
}
