﻿using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos._Base;

namespace Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos
{
   public class PrecioFiltrosConsulta : BaseFiltrosConsulta
   {
      public string Tienda { get; set; }
      public string Articulo { get; set; }
      public decimal Precio { get; set; }
   }
}
