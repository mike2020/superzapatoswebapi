﻿using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos._Base;

namespace Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos
{
   public class InventarioFiltrosConsulta : BaseFiltrosConsulta
   {
      public string Tienda { get; set; }
      public string Articulo { get; set; }
      public int TotalEnEstante { get; set; }
      public int TotalEnBodega { get; set; }
   }
}
