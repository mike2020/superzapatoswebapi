﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._08Opciones
{
   public class PaginacionOpciones
   {
      public int DefaultPageSize { get; set; }
      public int DefaultPageNumber { get; set; }
   }
}
