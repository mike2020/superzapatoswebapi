﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._08Opciones.Seguridad
{
   public class AutenticacionOpciones
   {
      public string Issuer { get; set; }
      public string Audience { get; set; }
      public string SecretKey { get; set; }
   }
}
