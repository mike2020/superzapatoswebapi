﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._04Dtos.Seguridad
{
   public class RolDto
   {
      public string Id { get; set; }
      public string Name { get; set; }
      public string Description { get; set; }
      public int Status { get; set; }
      public DateTime CreationDate { get; set; }
   }
}
