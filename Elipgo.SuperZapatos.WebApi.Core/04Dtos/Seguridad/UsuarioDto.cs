﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._04Dtos.Seguridad
{
   public class UsuarioDto
   {
      public string Id { get; set; }
      public string Name { get; set; }
      public string Password { get; set; }
      public string NameUser { get; set; }
      public string LastNameUser { get; set; }
      public string MothersLastNameUser { get; set; }
      public string EmailUser { get; set; }
      public string RoleId { get; set; }
      public int Status { get; set; }
      public string CreationDate { get; set; }
   }
}
