﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._04Dtos.Modulos
{
   public class InventarioDto
   {
      public int StoreId { get; set; }
      public int ArticleId { get; set; }
      public int TotalInShelf { get; set; }
      public int TotalInVault { get; set; }
      public int Status { get; set; }
      public string CreationBy { get; set; }
      public DateTime CreationDate { get; set; }
      public string LastUpdateBy { get; set; }
      //public DateTime? LastUpdateDate { get; set; }
   }
}
