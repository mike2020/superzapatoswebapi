﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._04Dtos.Catalogos
{
   public class TiendaDto
   {
      /// <summary>
      /// Identificador ustogenerado.
      /// </summary>
      public int Id { get; set; }
      public string Name { get; set; }
      public string Direction { get; set; }
      public int Status { get; set; }
      public string CreationBy { get; set; }
      public DateTime CreationDate { get; set; }
      public string LastUpdateBy { get; set; }
      //public DateTime? LastUpdateDate { get; set; }

   }
}
