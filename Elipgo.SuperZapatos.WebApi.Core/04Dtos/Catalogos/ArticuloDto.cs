﻿using System;

namespace Elipgo.SuperZapatos.WebApi.Core._04Dtos.Catalogos
{
   public class ArticuloDto
   {
      /// <summary>
      /// Identificador autogenerado.
      /// </summary>
      public int Id { get; set; }
      public string Name { get; set; }
      public string Description { get; set; }
      public int Status { get; set; }
      public string CreationBy { get; set; }
      public DateTime CreationDate { get; set; }
      public string LastUpdateBy { get; set; }
      //public DateTime? LastUpdateDate { get; set; }
   }
}
