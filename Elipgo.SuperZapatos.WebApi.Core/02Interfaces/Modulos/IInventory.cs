﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces._Base;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Modulos
{
   public interface IInventory : IBaseControl
   {
      int Id { get; set; }
      int TotalInShelf { get; set; }
      int TotalInVault { get; set; }
   }
}
