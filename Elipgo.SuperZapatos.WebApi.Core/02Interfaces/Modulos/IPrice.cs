﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces._Base;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Modulos
{
   public interface IPrice : IBaseControl
   {
      int Id { get; set; }

      int ArticleId { get; set; }

      decimal Amount { get; set; }
   }
}
