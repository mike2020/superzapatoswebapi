﻿using System;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces._Base
{
   public interface IBaseControl
   {
      int Status { get; set; }
      string CreationBy { get; set; }
      DateTime CreationDate { get; set; }
      string LastUpdateBy { get; set; }
      DateTime? LastUpdateDate { get; set; }
   }
}
