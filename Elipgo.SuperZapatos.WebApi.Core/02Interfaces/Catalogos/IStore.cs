﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces._Base;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Catalogos
{
   public interface IStore : IBaseEntidad, IBaseControl
   {
      string Direction { get; set; }
   }
}
