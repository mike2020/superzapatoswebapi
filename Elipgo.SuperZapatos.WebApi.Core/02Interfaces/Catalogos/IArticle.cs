﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces._Base;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Catalogos
{
   public interface IArticle : IBaseEntidad, IBaseControl
   {
      string Description { get; set; }
   }
}
