﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad._Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad._Base
{
   public interface IBaseRepositorioSeguridad<T>  where T : IBaseSeguridad
   {
      Task<IEnumerable<T>> ObtenerTodos();
      Task<T> ObtenerPorId(string id);
      Task<T> ObtenerPorNombre(string nombre);
      Task Agregar(T entidad);
      Task Actualizar(T entidad);
      Task Eliminar(string id);
   }
}
