﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._08Opciones.Seguridad;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad
{
   public interface ITokenRepositorioSeguridad
   {
      Task<string> GenerarToken(InicioSesion inicioSesion, AutenticacionOpciones opciones);
   }
}
