﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad._Base;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad
{
   public interface IRolApiRepositorioSeguridad : IBaseRepositorioSeguridad<RoleApi>
   {
   }
}
