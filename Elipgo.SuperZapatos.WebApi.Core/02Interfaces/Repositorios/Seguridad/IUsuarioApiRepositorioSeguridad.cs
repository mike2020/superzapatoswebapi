﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad._Base;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad
{
   public interface IUsuarioApiRepositorioSeguridad : IBaseRepositorioSeguridad<UserApi>
   {
      Task<UserApi> ObtenerCredenciales(InicioSesion inicioSesion);
   }
}