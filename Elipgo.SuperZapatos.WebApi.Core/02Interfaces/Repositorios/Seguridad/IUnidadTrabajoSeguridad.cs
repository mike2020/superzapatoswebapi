﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad._Base;
using System;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad
{
   public interface IUnidadTrabajoSeguridad : IDisposable
   {
      IBaseRepositorioSeguridad<User> UsuarioBaseRepositorioSeguridad { get; }
      IBaseRepositorioSeguridad<Role> RolBaseRepositorioSeguridad { get; }
      //IBaseRepositorioSeguridad<UserApi> UsuarioApiBaseRepositorioSeguridad { get; }
      //IBaseRepositorioSeguridad<RoleApi> RolApiBaseRepositorioSeguridad { get; }
      IUsuarioApiRepositorioSeguridad UsuarioApiRepositorioSeguridad { get; }
      IRolApiRepositorioSeguridad RolApiRepositorioSeguridad { get; }

      void SaveChanges();

      Task SaveChangesAsync();
   }
}
