﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces._Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios
{
   public interface IRepositorio<T> where T : IBaseEntidad
   {
      IEnumerable<T> ObtenerTodos();
      Task<T> ObtenerPorId(int id);
      Task Agregar(T entidad);
      void Actualizar(T entidad);
      Task Eliminar(int id);
   }
}
