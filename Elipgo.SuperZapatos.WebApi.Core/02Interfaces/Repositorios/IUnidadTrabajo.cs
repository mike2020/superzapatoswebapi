﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios;
using System;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios
{
   public interface IUnidadTrabajo : IDisposable
   {
      IRepositorio<Article> ArticuloRepositorio { get; }
      IRepositorio<Store> TiendaRepositorio { get; }
      IRepositorio<Inventory> InventarioRepositorio { get; }
      IRepositorio<Price> PrecioRepositorio { get; }

      void SaveChanges();

      Task SaveChangesAsync();
   }
}
