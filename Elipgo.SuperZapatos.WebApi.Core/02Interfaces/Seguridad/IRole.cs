﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad._Base;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad
{
   public interface IRole : IBaseSeguridad, IBaseControlSeguridad
   {
      string Description { get; set; }
   }
}
