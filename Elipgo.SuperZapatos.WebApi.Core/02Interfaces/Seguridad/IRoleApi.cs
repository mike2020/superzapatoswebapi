﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad._Base;
using System.Collections.Generic;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad
{
   public interface IRoleApi : IBaseSeguridad, IBaseControlSeguridad
   {
      string Description { get; set; }
   }
}