﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad._Base;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad
{
   public interface IUser : IBaseSeguridad, IBaseControlSeguridad
   {
      string Password { get; set; }
      string NameUser { get; set; }
      string LastNameUser { get; set; }
      string MothersLastNameUser { get; set; }
      string EmailUser { get; set; }
   }
}
