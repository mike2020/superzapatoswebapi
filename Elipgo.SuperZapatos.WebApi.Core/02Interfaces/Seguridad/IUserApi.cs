﻿using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad._Base;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad
{
   public interface IUserApi : IBaseSeguridad, IBaseControlSeguridad
   {
      string EmailUser { get; set; }
      string LastNameUser { get; set; }
      string MothersLastNameUser { get; set; }
      string NameUser { get; set; }
      string Password { get; set; }
      string RoleApiId { get; set; }
   }
}