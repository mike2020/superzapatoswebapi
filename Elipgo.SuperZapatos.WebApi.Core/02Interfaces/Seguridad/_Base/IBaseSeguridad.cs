﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad._Base
{
   public interface IBaseSeguridad
   {
      string Id { get; set; }
      string Name { get; set; }
   }
}
