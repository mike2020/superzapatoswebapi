﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Seguridad._Base
{
   public interface IBaseControlSeguridad
   {
      int Status { get; set; }
      DateTime CreationDate { get; set; }
   }
}
