﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Core._05Excepciones
{
   public class ExcepcionNegocio : Exception
   {
      public ExcepcionNegocio()
      {

      }

      public ExcepcionNegocio(string mensaje) : base(mensaje)
      {

      }
   }
}
