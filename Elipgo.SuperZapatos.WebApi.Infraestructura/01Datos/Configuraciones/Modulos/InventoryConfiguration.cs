﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Configuraciones.Modulos
{
   public class InventoryConfiguration : IEntityTypeConfiguration<Inventory>
   {
      public void Configure(EntityTypeBuilder<Inventory> builder)
      {
         builder.HasIndex(c => c.ArticleId).HasName("ix_Modules_Inventories_ArticleId");
         builder.HasIndex(c => c.StoreId).HasName("ix_Modules_Inventories_StoreId");
         builder.HasIndex(c => c.Status).HasName("ix_Modules_Inventories_Status");
         builder.HasIndex(c => new { c.StoreId, c.ArticleId }).HasName("ix_Modules_Inventories_StoreId_ArticleId").IsUnique();
         builder.Property(c => c.Status).HasDefaultValueSql("1");
         builder.Property(c => c.CreationBy).IsUnicode(false);
         builder.Property(c => c.CreationDate).HasDefaultValueSql("getdate()");
         builder.Property(c => c.LastUpdateBy).IsUnicode(false);
      }
   }
}
