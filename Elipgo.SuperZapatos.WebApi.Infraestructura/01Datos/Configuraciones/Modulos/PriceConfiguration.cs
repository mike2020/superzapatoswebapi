﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Configuraciones.Modulos
{
   public class PriceConfiguration : IEntityTypeConfiguration<Price>
   {
      public void Configure(EntityTypeBuilder<Price> builder)
      {
         builder.Property(c => c.ArticleId).IsRequired();
         builder.Property(c => c.ArticleId).HasColumnType("int");
         builder.HasIndex(c => c.ArticleId).HasName("ix_Modules_Prices_ArticleId");

         builder.Property(c => c.StoreId).IsRequired();
         builder.Property(c => c.StoreId).HasColumnType("int");
         builder.HasIndex(c => c.StoreId).HasName("ix_Modules_Prices_StoreId");

         builder.Property(c => c.Status).IsRequired();
         builder.Property(c => c.Status).HasColumnType("int");
         builder.Property(c => c.Status).HasDefaultValueSql("1");
         builder.HasIndex(c => c.Status).HasName("ix_Modules_Prices_Status");

         builder.Property(c => c.CreationBy).IsRequired();
         builder.Property(c => c.CreationBy).HasColumnType("nvarchar(450)");
         builder.Property(c => c.CreationBy).IsUnicode(false);

         builder.Property(c => c.CreationDate).IsRequired();
         builder.Property(c => c.CreationDate).HasColumnType("datetime");
         builder.Property(c => c.CreationDate).HasDefaultValueSql("getdate()");

         builder.Property(c => c.LastUpdateBy).HasColumnType("nvarchar(450)");
         builder.Property(c => c.LastUpdateBy).IsUnicode(false);

         builder.Property(c => c.LastUpdateDate).HasColumnType("datetime");

         builder.HasIndex(c => new { c.StoreId, c.ArticleId }).HasName("ix_Modules_Prices_StoreId_ArticleId").IsUnique();
      }
   }
}
