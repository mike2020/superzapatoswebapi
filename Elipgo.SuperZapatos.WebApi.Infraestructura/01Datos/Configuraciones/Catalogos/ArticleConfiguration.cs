﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Configuraciones.Catalogos
{
   public class ArticleConfiguration : IEntityTypeConfiguration<Article>
   {
      public void Configure(EntityTypeBuilder<Article> builder)
      {
         builder.HasIndex(c => c.Name).HasName("ix_Catalogs_Articles_Name").IsUnique();
         builder.Property(c => c.Name).IsUnicode(false);
         builder.Property(c => c.Description).IsUnicode(false);
         builder.Property(c => c.Status).HasDefaultValueSql("1");
         builder.Property(c => c.CreationBy).IsUnicode(false);
         builder.Property(c => c.CreationDate).HasDefaultValueSql("getdate()");
         builder.Property(c => c.LastUpdateBy).IsUnicode(false);
      }
   }
}
