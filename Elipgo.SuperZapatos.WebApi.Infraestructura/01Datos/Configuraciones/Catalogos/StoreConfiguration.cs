﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Configuraciones.Catalogos
{
   class StoreConfiguration : IEntityTypeConfiguration<Store>
   {
      public void Configure(EntityTypeBuilder<Store> builder)
      {
         builder.HasIndex(c => c.Name).HasName("ix_Catalogs_Stores_Name").IsUnique();
         builder.Property(c => c.Name).IsUnicode(false);
         builder.Property(c => c.Direction).IsUnicode(false);
         builder.Property(c => c.Status).HasDefaultValueSql("1");
         builder.Property(c => c.CreationBy).IsUnicode(false);
         builder.Property(c => c.CreationDate).HasDefaultValueSql("getdate()");
         builder.Property(c => c.LastUpdateBy).IsUnicode(false);
      }
   }
}
