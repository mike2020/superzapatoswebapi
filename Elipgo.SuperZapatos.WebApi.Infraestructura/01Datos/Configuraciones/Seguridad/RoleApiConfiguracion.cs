﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Configuraciones.Seguridad
{
   public class RoleApiConfiguration : IEntityTypeConfiguration<RoleApi>
   {
      public void Configure(EntityTypeBuilder<RoleApi> builder)
      {
         builder.HasIndex(c => c.Name).HasName("ix_Security_RolesApi_Name").IsUnique();
         builder.Property(c => c.Name).IsUnicode(false);
         builder.Property(c => c.Description).IsUnicode(false);
         builder.Property(c => c.Status).HasDefaultValueSql("1");
         builder.Property(c => c.CreationDate).HasDefaultValueSql("getdate()");
      }
   }
}
