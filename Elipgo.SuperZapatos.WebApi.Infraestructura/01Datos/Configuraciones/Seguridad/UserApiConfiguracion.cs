﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Configuraciones.Seguridad
{
   public class UserApiConfiguration : IEntityTypeConfiguration<UserApi>
   {
      public void Configure(EntityTypeBuilder<UserApi> builder)
      {
         builder.HasIndex(c => c.Name).HasName("ix_Security_UsersApi_Name").IsUnique();
         builder.HasIndex(c => c.EmailUser).HasName("ix_Security_UsersApi_EmailUser").IsUnique();
         builder.HasIndex(c => c.LastNameUser).HasName("ix_Security_UsersApi_LastNameUser");
         builder.HasIndex(c => c.MothersLastNameUser).HasName("ix_Security_UsersApi_MothersLastNameUser");
         builder.HasIndex(c => c.NameUser).HasName("ix_Security_UsersApi_NameUser");
         builder.HasIndex(c => c.RoleApiId).HasName("ix_Security_UsersApi_RolId");
         builder.Property(c => c.Name).IsUnicode(false);
         builder.Property(c => c.Password).IsUnicode(false);
         builder.Property(c => c.NameUser).IsUnicode(false);
         builder.Property(c => c.LastNameUser).IsUnicode(false);
         builder.Property(c => c.MothersLastNameUser).IsUnicode(false);
         builder.Property(c => c.EmailUser).IsUnicode(false);
         builder.Property(c => c.Status).HasDefaultValueSql("1");
         builder.Property(c => c.CreationDate).HasDefaultValueSql("getdate()");
      }
   }
}
