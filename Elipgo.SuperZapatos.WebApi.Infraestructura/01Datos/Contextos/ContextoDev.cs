﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
//using Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Configuraciones.Catalogos;
//using Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Configuraciones.Modulos;
//using Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Configuraciones.Seguridad;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Contextos
{
   public class ContextoDev : DbContext
   //public class ContextoDev : IdentityDbContext
   {
      //public ContextoDev()
      //{

      //}

      public ContextoDev(DbContextOptions<ContextoDev> options) : base(options)
      {

      }

      #region   < < <   E s q u e m a   C a t á l o g o s   > > >
      public DbSet<Article> Articles { get; set; }
      public DbSet<Store> Stores { get; set; }
      #endregion

      #region   < < <   E s q u e m a   M ó d u l o s   > > >
      public DbSet<Price> Prices { get; set; }
      public DbSet<Inventory> Inventories { get; set; }
      #endregion

      #region   < < <   E s q u e m a   S e g u r i d a d   > > >
      public DbSet<User> Users { get; set; }
      public DbSet<Role> Roles { get; set; }
      public DbSet<User> UsersApi { get; set; }
      public DbSet<Role> RolesApi { get; set; }
      #endregion

      protected override void OnModelCreating(ModelBuilder modelBuilder)
      {
         base.OnModelCreating(modelBuilder);

         //#region   < < <   E s q u e m a   C a t á l o g o s   > > >
         //modelBuilder.ApplyConfiguration(new ArticleConfiguration());
         //modelBuilder.ApplyConfiguration(new StoreConfiguration());
         //#endregion

         //#region   < < <   E s q u e m a   M ó d u l o s   > > >
         //modelBuilder.ApplyConfiguration(new InventoryConfiguration());
         //modelBuilder.ApplyConfiguration(new PriceConfiguration());
         //#endregion

         //#region   < < <   E s q u e m a   S e g u r i d a d   > > >
         //modelBuilder.ApplyConfiguration(new UserConfiguration());
         //modelBuilder.ApplyConfiguration(new RoleConfiguration());
         //modelBuilder.ApplyConfiguration(new UserApiConfiguration());
         //modelBuilder.ApplyConfiguration(new RoleApiConfiguration());
         //#endregion

         /*---   Con esta linea se aplican todas las configuraciones de las entidades y se evita hacerlo una por una ---*/
         modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
      }
   }
}
