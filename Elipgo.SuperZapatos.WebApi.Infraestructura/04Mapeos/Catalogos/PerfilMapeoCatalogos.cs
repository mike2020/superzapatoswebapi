﻿using AutoMapper;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._04Dtos.Catalogos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._04Mapeos.Catalogos
{
   public class PerfilMapeoCatalogos : Profile
   {
      public PerfilMapeoCatalogos()
      {
         CreateMap<Article, ArticuloDto>();
         CreateMap<ArticuloDto, Article>();

         CreateMap<Store, TiendaDto>();
         CreateMap<TiendaDto, Store>();
      }

      
   }
}
