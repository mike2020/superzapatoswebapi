﻿using AutoMapper;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._04Dtos.Modulos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._04Mapeos.Modulos
{
   public class PerfilMapeoModulos : Profile
   {
      public PerfilMapeoModulos()
      {
         CreateMap<Inventory, InventarioDto>();
         CreateMap<InventarioDto, Inventory>();

         CreateMap<Price, PrecioDto>();
         CreateMap<PrecioDto, Price>();
      }
   }
}
