﻿using AutoMapper;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._04Dtos.Seguridad;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._04Mapeos.Seguridad
{
   public class PerfilMapeoSeguridad : Profile
   {
      public PerfilMapeoSeguridad()
      {
         CreateMap<User, UsuarioDto>();
         CreateMap<UsuarioDto, User>();

         CreateMap<Role, RolDto>();
         CreateMap<RolDto, Role>();

         CreateMap<UserApi, UsuarioApiDto>().ReverseMap();
         //CreateMap<UsuarioApiDto, UserApi>();

         CreateMap<RoleApi, RolApiDto>().ReverseMap();
         //CreateMap<RolApiDto, RoleApi>();
      }
   }
}
