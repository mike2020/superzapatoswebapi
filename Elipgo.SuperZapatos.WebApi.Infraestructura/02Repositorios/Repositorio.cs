﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades._Base;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios;
using Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Contextos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._02Repositorios
{
   public class Repositorio<T> : IRepositorio<T> where T : BaseEntidad
   {
      private readonly ContextoDev _contexto;
      private readonly DbSet<T> _entidades;

      public Repositorio(ContextoDev contexto)
      {
         _contexto = contexto;
         _entidades = _contexto.Set<T>();
      }

      public IEnumerable<T> ObtenerTodos()
      {
         return _entidades.AsEnumerable();
      }

      public async Task<T> ObtenerPorId(int id)
      {
         return await _entidades.FindAsync(id);
      }

      public async Task Agregar(T entidad)
      {
         await _entidades.AddAsync(entidad);
      }

      public void Actualizar(T entidad)
      {
         _entidades.Update(entidad);
      }

      public async Task Eliminar(int id)
      {
         T entidad = await ObtenerPorId(id);
         _entidades.Remove(entidad);
      }
   }
}
