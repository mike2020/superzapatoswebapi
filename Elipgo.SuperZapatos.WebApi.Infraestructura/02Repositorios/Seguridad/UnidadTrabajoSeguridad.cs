﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad._Base;
using Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Contextos;
using Elipgo.SuperZapatos.WebApi.Infraestructura._02Repositorios.Seguridad._Base;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._02Repositorios.Seguridad
{
   public class UnidadTrabajoSeguridad : IUnidadTrabajoSeguridad
   {
      private readonly ContextoDev _contexto;
      private readonly IBaseRepositorioSeguridad<User> _usuarioBaseRepositorioSeguridad;
      private readonly IBaseRepositorioSeguridad<Role> _rolBaseRepositorioSeguridad;
      private readonly IBaseRepositorioSeguridad<UserApi> _usuarioApiBaseRepositorioSeguridad;
      private readonly IBaseRepositorioSeguridad<RoleApi> _rolApiBaseRepositorioSeguridad;
      private readonly IUsuarioApiRepositorioSeguridad _usuarioApiRepositorioSeguridad;
      private readonly IRolApiRepositorioSeguridad _rolApiRepositorioSeguridad;

      public UnidadTrabajoSeguridad(ContextoDev contexto,
                                    IBaseRepositorioSeguridad<User> usuarioBaseRepositorioSeguridad,
                                    IBaseRepositorioSeguridad<Role> rolBaseRepositorioSeguridad,
                                    IBaseRepositorioSeguridad<UserApi> usuarioApiBaseRepositorioSeguridad,
                                    IBaseRepositorioSeguridad<RoleApi> rolApiBaseRepositorioSeguridad,
                                    IUsuarioApiRepositorioSeguridad usuarioApiRepositorioSeguridad,
                                    IRolApiRepositorioSeguridad rolApiRepositorioSeguridad
                                   )
      {
         _contexto = contexto;
         _usuarioBaseRepositorioSeguridad = usuarioBaseRepositorioSeguridad;
         _rolBaseRepositorioSeguridad = rolBaseRepositorioSeguridad;
         _usuarioApiBaseRepositorioSeguridad = usuarioApiBaseRepositorioSeguridad;
         _rolApiBaseRepositorioSeguridad = rolApiBaseRepositorioSeguridad;
         _usuarioApiRepositorioSeguridad = usuarioApiRepositorioSeguridad;
         _rolApiRepositorioSeguridad = rolApiRepositorioSeguridad;
      }

      public IBaseRepositorioSeguridad<User> UsuarioBaseRepositorioSeguridad => _usuarioBaseRepositorioSeguridad ?? new BaseRepositorioSeguridad<User>(_contexto);

      public IBaseRepositorioSeguridad<Role> RolBaseRepositorioSeguridad => _rolBaseRepositorioSeguridad ?? new BaseRepositorioSeguridad<Role>(_contexto);

      //public IBaseRepositorioSeguridad<UserApi> UsuarioApiBaseRepositorioSeguridad => _usuarioApiBaseRepositorioSeguridad ?? new BaseRepositorioSeguridad<UserApi>(_contexto);

      //public IBaseRepositorioSeguridad<RoleApi> RolApiBaseRepositorioSeguridad => _rolApiBaseRepositorioSeguridad ?? new BaseRepositorioSeguridad<RoleApi>(_contexto);

      public IUsuarioApiRepositorioSeguridad UsuarioApiRepositorioSeguridad => _usuarioApiRepositorioSeguridad ?? new UsuarioApiRepositorioSeguridad(_contexto);

      public IRolApiRepositorioSeguridad RolApiRepositorioSeguridad => _rolApiRepositorioSeguridad ?? new RolApiRepositorioSeguridad(_contexto);

      public void Dispose()
      {
         if (_contexto != null)
         {
            _contexto.Dispose();
         }
      }

      public void SaveChanges()
      {
         _contexto.SaveChanges();
      }

      public async Task SaveChangesAsync()
      {
         await _contexto.SaveChangesAsync();
      }
   }
}
