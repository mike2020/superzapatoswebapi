﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._08Opciones.Seguridad;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._02Repositorios.Seguridad
{
   public class TokenRepositorioSeguridad : ITokenRepositorioSeguridad
   {
      //private readonly AutenticacionOpciones _autenticacionOpciones;
      private readonly IUnidadTrabajoSeguridad _unidadTrabajoSeguridad;

      public TokenRepositorioSeguridad(
                              //IOptions<AutenticacionOpciones> autenticacionOpciones,
                              IUnidadTrabajoSeguridad unidadTrabajoSeguridad
                             )
      {
         //_autenticacionOpciones = autenticacionOpciones.Value;
         _unidadTrabajoSeguridad = unidadTrabajoSeguridad;
      }  

      public async Task<string> GenerarToken(InicioSesion inicioSesion, AutenticacionOpciones opciones)
      {
         /*---   Se debe incrementar la seguridad en la informacion del ususrio   ---*/
         var usuario = await _unidadTrabajoSeguridad.UsuarioBaseRepositorioSeguridad.ObtenerPorNombre(inicioSesion.Usuario);
         //Header
         var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(opciones.SecretKey));
         var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
         var header = new JwtHeader(signingCredentials);

         //Cleamis
         var claims = new[] 
         {
            new Claim(ClaimTypes.Name, Convert.ToString(usuario.NameUser + " " + usuario.LastNameUser + " " + usuario.MothersLastNameUser).Trim()),
            new Claim(ClaimTypes.Email, Convert.ToString(usuario.EmailUser)),
            new Claim(ClaimTypes.Role, "Administrador"), /*---   Se debe Obtener   ---*/
         };

         //Payload
         var payload = new JwtPayload
         {
            { "Issuer", opciones.Issuer },
            { "Audience", opciones.Audience },
            { "Claims", claims },
            { "LifeTime", DateTime.Now },
            { "Expiration",  DateTime.UtcNow.AddMinutes(2) }
         };

         var token = new JwtSecurityToken(header, payload);

         return new JwtSecurityTokenHandler().WriteToken(token);
      }
   }
}
