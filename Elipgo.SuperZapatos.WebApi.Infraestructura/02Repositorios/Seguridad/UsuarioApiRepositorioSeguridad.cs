﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Contextos;
using Elipgo.SuperZapatos.WebApi.Infraestructura._02Repositorios.Seguridad._Base;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._02Repositorios.Seguridad
{
   public class UsuarioApiRepositorioSeguridad : BaseRepositorioSeguridad<UserApi>, IUsuarioApiRepositorioSeguridad
   {
      public UsuarioApiRepositorioSeguridad(ContextoDev contexto) : base(contexto)
      {

      }

      public async Task<UserApi> ObtenerCredenciales(InicioSesion inicioSesion)
      {
         return await _entidades.FirstOrDefaultAsync(u => u.Name == inicioSesion.Usuario && u.Password == inicioSesion.Contrasenia);
      }
   }
}
