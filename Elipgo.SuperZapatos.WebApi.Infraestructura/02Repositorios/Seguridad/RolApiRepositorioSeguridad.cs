﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Contextos;
using Elipgo.SuperZapatos.WebApi.Infraestructura._02Repositorios.Seguridad._Base;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._02Repositorios.Seguridad
{
   public class RolApiRepositorioSeguridad : BaseRepositorioSeguridad<RoleApi>, IRolApiRepositorioSeguridad
   {
      public RolApiRepositorioSeguridad(ContextoDev contexto) : base(contexto)
      {

      }
   }
}
