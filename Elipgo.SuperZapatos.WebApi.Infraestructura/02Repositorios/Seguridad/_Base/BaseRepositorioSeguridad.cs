﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad._Base;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad._Base;
using Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Contextos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._02Repositorios.Seguridad._Base
{
   public class BaseRepositorioSeguridad<T> : IBaseRepositorioSeguridad<T> where T : BaseSeguridad
   {
      private readonly ContextoDev _contexto;
      protected readonly DbSet<T> _entidades;

      public BaseRepositorioSeguridad(ContextoDev contexto)
      {
         _contexto = contexto;
         _entidades = contexto.Set<T>();
      }

      public async Task<IEnumerable<T>> ObtenerTodos()
      {
         return await _entidades.ToListAsync();
      }

      public async Task<T> ObtenerPorId(string id)
      {
         return await _entidades.FindAsync(id);
      }

      public async Task<T> ObtenerPorNombre(string nombre)
      {
         return await _entidades.FirstOrDefaultAsync(u => u.Name == nombre);
      }

      public async Task Agregar(T entidad)
      {
         entidad.Id = GenerarNuevoId();

         _entidades.Add(entidad);
         await _contexto.SaveChangesAsync();
      }

      public async Task Actualizar(T entidad)
      {
         _entidades.Update(entidad);
         await _contexto.SaveChangesAsync();
      }

      public async Task Eliminar(string id)
      {
         T entidad = await ObtenerPorId(id);
         _entidades.Remove(entidad);
         await _contexto.SaveChangesAsync();
      }

      private string GenerarNuevoId()
      {
         var buffer = Guid.NewGuid().ToByteArray();

         var tiempo = new DateTime(0x76c, 1, 1);
         var ahora = DateTime.Now;
         var span = new TimeSpan(ahora.Ticks - tiempo.Ticks);
         var hora = ahora.TimeOfDay;

         var bytes = BitConverter.GetBytes(span.Days);
         var arreglo = BitConverter.GetBytes(
               (long)(hora.TotalMilliseconds / 3.333333));

         Array.Reverse(bytes);
         Array.Reverse(arreglo);
         Array.Copy(bytes, bytes.Length - 2, buffer, buffer.Length - 6, 2);
         Array.Copy(arreglo, arreglo.Length - 4, buffer, buffer.Length - 4, 4);

         return new Guid(buffer).ToString();
      }
   }
}
