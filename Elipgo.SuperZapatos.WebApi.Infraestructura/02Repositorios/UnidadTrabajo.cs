﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios;
using Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Contextos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._02Repositorios
{
   public class UnidadTrabajo : IUnidadTrabajo
   {
      private readonly ContextoDev _contexto;
      private readonly IRepositorio<Article> _articuloRepositorio;
      private readonly IRepositorio<Store> _tiendaRepositorio;
      private readonly IRepositorio<Inventory> _inventarioRepositorio;
      private readonly IRepositorio<Price> _precioRepositorio;

      public UnidadTrabajo(ContextoDev contexto,
                           IRepositorio<Article> articuloRepositorio,
                           IRepositorio<Store> tiendaRepositorio,
                           IRepositorio<Inventory> inventarioRepositorio,
                           IRepositorio<Price> precioRepositorio)
      {
         _contexto = contexto;
         _articuloRepositorio = articuloRepositorio;
         _tiendaRepositorio = tiendaRepositorio;
         _inventarioRepositorio = inventarioRepositorio;
         _precioRepositorio = precioRepositorio;
      }

      public IRepositorio<Article> ArticuloRepositorio => _articuloRepositorio ?? new Repositorio<Article>(_contexto);

      public IRepositorio<Store> TiendaRepositorio => _tiendaRepositorio ?? new Repositorio<Store>(_contexto);

      public IRepositorio<Inventory> InventarioRepositorio => _inventarioRepositorio ?? new Repositorio<Inventory>(_contexto);

      public IRepositorio<Price> PrecioRepositorio => _precioRepositorio ?? new Repositorio<Price>(_contexto);

      public void Dispose()
      {
         if (_contexto != null) {
            _contexto.Dispose();
         }
      }

      public void SaveChanges()
      {
         _contexto.SaveChanges();
      }

      public async Task SaveChangesAsync()
      {
         await _contexto.SaveChangesAsync();
      }
   }
}
