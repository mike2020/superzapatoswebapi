﻿using Elipgo.SuperZapatos.WebApi.Core._05Excepciones;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._03Filtros
{
   public class FitroExcepcionGlobal : IExceptionFilter
   {
      public void OnException(ExceptionContext context)
      {
         if (context.Exception.GetType() == typeof(ExcepcionNegocio)) {
            var excepcion = (ExcepcionNegocio)context.Exception;
            var validacion = new
            {
               success = false,
               error_code = 400,
               error_title = "Bad request",
               error_msg = excepcion.Message
            };

            var json = new
            {
               errores = new [] { validacion }
            };

            context.Result = new BadRequestObjectResult(json);
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            context.ExceptionHandled = true;
         }
      }
   }
}
