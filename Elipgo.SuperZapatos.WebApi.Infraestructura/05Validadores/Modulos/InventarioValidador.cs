﻿using Elipgo.SuperZapatos.WebApi.Core._04Dtos.Modulos;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._05Validadores.Modulos
{
   public class InventarioValidador : AbstractValidator<InventarioDto>
   {
      public InventarioValidador()
      {
         RuleFor(e => e.StoreId)
            .NotNull();

         RuleFor(e => e.ArticleId)
            .NotNull();

         RuleFor(e => e.TotalInShelf)
            .NotNull();

         RuleFor(e => e.TotalInVault)
            .NotNull();

         RuleFor(e => e.Status)
            .NotNull();
      }
   }
}
