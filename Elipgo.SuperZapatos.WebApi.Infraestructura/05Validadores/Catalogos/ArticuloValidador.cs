﻿using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._04Dtos.Catalogos;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._05Validadores.Catalogos
{
   public class ArticuloValidador : AbstractValidator<ArticuloDto>
   {
      public ArticuloValidador()
      {
         RuleFor(e => e.Name)
            .NotNull()
            .Length(10, 150);

         RuleFor(e => e.Description)
            .NotNull()
            .Length(10, 200);

         RuleFor(e => e.Status)
                     .NotNull();
      }
   }
}
