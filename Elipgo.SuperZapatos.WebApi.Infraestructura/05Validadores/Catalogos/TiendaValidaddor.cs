﻿using Elipgo.SuperZapatos.WebApi.Core._04Dtos.Catalogos;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._05Validadores.Catalogos
{
   public class TiendaValidaddor : AbstractValidator<TiendaDto>
   {
      public TiendaValidaddor()
      {
         RuleFor(e => e.Name)
            .NotNull()
            .Length(10, 150);

         RuleFor(e => e.Direction)
            .NotNull()
            .Length(10, 200);

         RuleFor(e => e.Status)
            .NotNull();
      }
   }
}
