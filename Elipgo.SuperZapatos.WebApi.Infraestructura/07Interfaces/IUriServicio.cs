﻿using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos;
using System;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._07Intefaces
{
   public interface IUriServicio
   {
      Uri ObtenerArticuloPaginacionUri(ArticuloFiltrosConsulta filtros, string accionUrl);
      Uri ObtenerTiendaPaginacionUri(TiendaFiltrosConsulta filtros, string accionUrl);
      Uri ObtenerInventarioPaginacionUri(InventarioFiltrosConsulta filtros, string accionUrl);
      Uri ObtenerPrecioPaginacionUri(PrecioFiltrosConsulta filtros, string accionUrl);
   }
}