﻿using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos;
using Elipgo.SuperZapatos.WebApi.Infraestructura._07Intefaces;
using System;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura._06Servicios
{
   public class UriServicio : IUriServicio
   {
      private readonly string _baseUri;

      public UriServicio(string baseUri)
      {
         _baseUri = baseUri;
      }

      public Uri ObtenerArticuloPaginacionUri(ArticuloFiltrosConsulta filtros, string accionUrl) {
         string baseUrl = $"{_baseUri}{accionUrl}";

         return new Uri(baseUrl);
      }

      public Uri ObtenerTiendaPaginacionUri(TiendaFiltrosConsulta filtros, string accionUrl)
      {
         string baseUrl = $"{_baseUri}{accionUrl}";

         return new Uri(baseUrl);
      }

      public Uri ObtenerInventarioPaginacionUri(InventarioFiltrosConsulta filtros, string accionUrl)
      {
         string baseUrl = $"{_baseUri}{accionUrl}";

         return new Uri(baseUrl);
      }

      public Uri ObtenerPrecioPaginacionUri(PrecioFiltrosConsulta filtros, string accionUrl)
      {
         string baseUrl = $"{_baseUri}{accionUrl}";

         return new Uri(baseUrl);
      }
   }
}
