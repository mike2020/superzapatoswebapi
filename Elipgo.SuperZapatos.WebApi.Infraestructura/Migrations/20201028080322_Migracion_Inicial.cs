﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Elipgo.SuperZapatos.WebApi.Infraestructura.Migrations
{
    public partial class Migracion_Inicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Catalogs");

            migrationBuilder.EnsureSchema(
                name: "Modules");

            migrationBuilder.EnsureSchema(
                name: "Security");

            migrationBuilder.CreateTable(
                name: "Articles",
                schema: "Catalogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<int>(type: "int", nullable: false, defaultValueSql: "1"),
                    CreationBy = table.Column<string>(type: "nvarchar(450)", unicode: false, nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "getdate()"),
                    LastUpdateBy = table.Column<string>(type: "nvarchar(450)", unicode: false, nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(150)", unicode: false, maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(200)", unicode: false, maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Stores",
                schema: "Catalogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<int>(type: "int", nullable: false, defaultValueSql: "1"),
                    CreationBy = table.Column<string>(type: "nvarchar(450)", unicode: false, nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "getdate()"),
                    LastUpdateBy = table.Column<string>(type: "nvarchar(450)", unicode: false, nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(150)", unicode: false, maxLength: 150, nullable: false),
                    Direction = table.Column<string>(type: "nvarchar(200)", unicode: false, maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                schema: "Security",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false, defaultValueSql: "1"),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "getdate()"),
                    Name = table.Column<string>(type: "nvarchar(320)", unicode: false, maxLength: 320, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(200)", unicode: false, maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RolesApi",
                schema: "Security",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false, defaultValueSql: "1"),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "getdate()"),
                    Name = table.Column<string>(type: "nvarchar(320)", unicode: false, maxLength: 320, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(200)", unicode: false, maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RolesApi", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Inventories",
                schema: "Modules",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<int>(type: "int", nullable: false, defaultValueSql: "1"),
                    CreationBy = table.Column<string>(type: "nvarchar(450)", unicode: false, nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "getdate()"),
                    LastUpdateBy = table.Column<string>(type: "nvarchar(450)", unicode: false, nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    TotalInShelf = table.Column<int>(type: "int", nullable: false),
                    TotalInVault = table.Column<int>(type: "int", nullable: false),
                    StoreId = table.Column<int>(type: "int", nullable: false),
                    ArticleId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inventories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Inventories_Articles_ArticleId",
                        column: x => x.ArticleId,
                        principalSchema: "Catalogs",
                        principalTable: "Articles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Inventories_Stores_StoreId",
                        column: x => x.StoreId,
                        principalSchema: "Catalogs",
                        principalTable: "Stores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Prices",
                schema: "Modules",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<int>(type: "int", nullable: false, defaultValueSql: "1"),
                    CreationBy = table.Column<string>(type: "nvarchar(450)", unicode: false, nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "getdate()"),
                    LastUpdateBy = table.Column<string>(type: "nvarchar(450)", unicode: false, nullable: true),
                    LastUpdateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Amount = table.Column<decimal>(type: "decimal(20,6)", nullable: false),
                    StoreId = table.Column<int>(type: "int", nullable: false),
                    ArticleId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Prices_Articles_ArticleId",
                        column: x => x.ArticleId,
                        principalSchema: "Catalogs",
                        principalTable: "Articles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Prices_Stores_StoreId",
                        column: x => x.StoreId,
                        principalSchema: "Catalogs",
                        principalTable: "Stores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "Security",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false, defaultValueSql: "1"),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "getdate()"),
                    Name = table.Column<string>(type: "nvarchar(320)", unicode: false, maxLength: 320, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(127)", unicode: false, maxLength: 127, nullable: false),
                    NameUser = table.Column<string>(type: "nvarchar(100)", unicode: false, maxLength: 100, nullable: false),
                    LastNameUser = table.Column<string>(type: "nvarchar(100)", unicode: false, maxLength: 100, nullable: false),
                    MothersLastNameUser = table.Column<string>(type: "nvarchar(100)", unicode: false, maxLength: 100, nullable: false),
                    EmailUser = table.Column<string>(type: "nvarchar(320)", unicode: false, maxLength: 320, nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Roles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "Security",
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UsersApi",
                schema: "Security",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false, defaultValueSql: "1"),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "getdate()"),
                    Name = table.Column<string>(type: "nvarchar(320)", unicode: false, maxLength: 320, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(127)", unicode: false, maxLength: 127, nullable: false),
                    NameUser = table.Column<string>(type: "nvarchar(100)", unicode: false, maxLength: 100, nullable: false),
                    LastNameUser = table.Column<string>(type: "nvarchar(100)", unicode: false, maxLength: 100, nullable: false),
                    MothersLastNameUser = table.Column<string>(type: "nvarchar(100)", unicode: false, maxLength: 100, nullable: false),
                    EmailUser = table.Column<string>(type: "nvarchar(320)", unicode: false, maxLength: 320, nullable: false),
                    RoleApiId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsersApi", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UsersApi_RolesApi_RoleApiId",
                        column: x => x.RoleApiId,
                        principalSchema: "Security",
                        principalTable: "RolesApi",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_Catalogs_Articles_Name",
                schema: "Catalogs",
                table: "Articles",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_Catalogs_Stores_Name",
                schema: "Catalogs",
                table: "Stores",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_Modules_Inventories_ArticleId",
                schema: "Modules",
                table: "Inventories",
                column: "ArticleId");

            migrationBuilder.CreateIndex(
                name: "ix_Modules_Inventories_Status",
                schema: "Modules",
                table: "Inventories",
                column: "Status");

            migrationBuilder.CreateIndex(
                name: "ix_Modules_Inventories_StoreId",
                schema: "Modules",
                table: "Inventories",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "ix_Modules_Inventories_StoreId_ArticleId",
                schema: "Modules",
                table: "Inventories",
                columns: new[] { "StoreId", "ArticleId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_Modules_Prices_ArticleId",
                schema: "Modules",
                table: "Prices",
                column: "ArticleId");

            migrationBuilder.CreateIndex(
                name: "ix_Modules_Prices_Status",
                schema: "Modules",
                table: "Prices",
                column: "Status");

            migrationBuilder.CreateIndex(
                name: "ix_Modules_Prices_StoreId",
                schema: "Modules",
                table: "Prices",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "ix_Modules_Prices_StoreId_ArticleId",
                schema: "Modules",
                table: "Prices",
                columns: new[] { "StoreId", "ArticleId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_Security_Roles_Name",
                schema: "Security",
                table: "Roles",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_Security_RolesApi_Name",
                schema: "Security",
                table: "RolesApi",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_Security_Users_EmailUser",
                schema: "Security",
                table: "Users",
                column: "EmailUser",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_Security_Users_LastNameUser",
                schema: "Security",
                table: "Users",
                column: "LastNameUser");

            migrationBuilder.CreateIndex(
                name: "ix_Security_Users_MothersLastNameUser",
                schema: "Security",
                table: "Users",
                column: "MothersLastNameUser");

            migrationBuilder.CreateIndex(
                name: "ix_Security_Users_Name",
                schema: "Security",
                table: "Users",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_Security_Users_NameUser",
                schema: "Security",
                table: "Users",
                column: "NameUser");

            migrationBuilder.CreateIndex(
                name: "ix_Security_Users_RolId",
                schema: "Security",
                table: "Users",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "ix_Security_UsersApi_EmailUser",
                schema: "Security",
                table: "UsersApi",
                column: "EmailUser",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_Security_UsersApi_LastNameUser",
                schema: "Security",
                table: "UsersApi",
                column: "LastNameUser");

            migrationBuilder.CreateIndex(
                name: "ix_Security_UsersApi_MothersLastNameUser",
                schema: "Security",
                table: "UsersApi",
                column: "MothersLastNameUser");

            migrationBuilder.CreateIndex(
                name: "ix_Security_UsersApi_Name",
                schema: "Security",
                table: "UsersApi",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_Security_UsersApi_NameUser",
                schema: "Security",
                table: "UsersApi",
                column: "NameUser");

            migrationBuilder.CreateIndex(
                name: "ix_Security_UsersApi_RolId",
                schema: "Security",
                table: "UsersApi",
                column: "RoleApiId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Inventories",
                schema: "Modules");

            migrationBuilder.DropTable(
                name: "Prices",
                schema: "Modules");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "UsersApi",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "Articles",
                schema: "Catalogs");

            migrationBuilder.DropTable(
                name: "Stores",
                schema: "Catalogs");

            migrationBuilder.DropTable(
                name: "Roles",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "RolesApi",
                schema: "Security");
        }
    }
}
