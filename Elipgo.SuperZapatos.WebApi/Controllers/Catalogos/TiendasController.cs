﻿using AutoMapper;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._04Dtos.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._06Servicios.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using Elipgo.SuperZapatos.WebApi.Infraestructura._07Intefaces;
using Elipgo.SuperZapatos.WebApi.Respuestas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Controllers.Catalogos
{
   [Authorize]
   [Produces("application/json")]
   [Route("api/[controller]")]
   [ApiController]
   public class TiendasController : ControllerBase
   {
      private readonly ITiendaServicio _tiendaServicio;
      private readonly IMapper _mapeador;
      private readonly IUriServicio _uriServicio;

      public TiendasController(
                               ITiendaServicio tiendaServicio,
                               IMapper mapeador,
                               IUriServicio uriServicio
                              )
      {
         _tiendaServicio = tiendaServicio;
         _mapeador = mapeador;
         _uriServicio = uriServicio;
      }

      /// <summary>
      /// Obtiene todos las tiendas.
      /// </summary>
      /// <param name="filtros">Filtros por aplicar.</param>
      /// <returns>Listado de tiendas</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<IEnumerable<TiendaDto>>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpGet(Name = nameof(ObtenerTiendas))]
      public IActionResult ObtenerTiendas([FromQuery] TiendaFiltrosConsulta filtros)
      {
         var tiendas = _tiendaServicio.ObtenerTiendas(filtros);
         var tiendasDto = _mapeador.Map<IEnumerable<TiendaDto>>(tiendas);

         /*---   Se muestra en la respuesta la información sobre la paginación   ---*/
         var metadata = new Metadata
         {
            TotalRenglones = tiendas.TotalRenglones,
            TamanioPagina = tiendas.TamanioPagina,
            PaginaActual = tiendas.PaginaActual,
            TotalPaginas = tiendas.TotalPaginas,
            TienePaginaSiguiente = tiendas.TienePaginaSiguiente,
            TienePaginaAnterior = tiendas.TienePaginaAnterior,
            UrlPaginaSiguiente = _uriServicio.ObtenerTiendaPaginacionUri(filtros, Url.RouteUrl(nameof(ObtenerTiendas))).ToString(),
            UrlPaginaAnterior = _uriServicio.ObtenerTiendaPaginacionUri(filtros, Url.RouteUrl(nameof(ObtenerTiendas))).ToString()
         };

         var respuesta = new RespuestaApi<IEnumerable<TiendaDto>>(tiendasDto)
         {
            Meta = metadata
         };

         return Ok(respuesta);
      }

      /// <summary>
      /// Obtiene una tienda a partir del Id.
      /// </summary>
      /// <param name="id">Identificador de la tienda</param>
      /// <returns>Tienda</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<TiendaDto>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpGet("{id}", Name = nameof(ObtenerTienda))]
      public async Task<IActionResult> ObtenerTienda(int id)
      {
         var tienda = await _tiendaServicio.ObtenerTienda(id);
         var tiendaDto = _mapeador.Map<TiendaDto>(tienda);

         var respuesta = new RespuestaApi<TiendaDto>(tiendaDto);

         return Ok(respuesta);
      }

      /// <summary>
      /// Agrega una tienda.
      /// </summary>
      /// <param name="tiendaDto">Tienda</param>
      /// <returns></returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<TiendaDto>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpPost(Name = nameof(AgregarTienda))]
      public async Task<IActionResult> AgregarTienda(TiendaDto tiendaDto)
      {
         var tienda = _mapeador.Map<Store>(tiendaDto);

         await _tiendaServicio.AgregarTienda(tienda);

         tiendaDto = _mapeador.Map<TiendaDto>(tienda);

         var respuesta = new RespuestaApi<TiendaDto>(tiendaDto);

         return Ok(respuesta);
      }

      /// <summary>
      /// Actualiza la información de la tienda.
      /// </summary>
      /// <param name="id">Identificador de la tienda.</param>
      /// <param name="tiendaDto">Tienda</param>
      /// <returns>true/false</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<bool>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpPut("{id}", Name = nameof(ActualizarTienda))]
      public async Task<IActionResult> ActualizarTienda(int id, TiendaDto tiendaDto)
      {
         var tienda = _mapeador.Map<Store>(tiendaDto);
         tienda.Id = id;

         var resultado = await _tiendaServicio.ActualizarTienda(tienda);
         var respuesta = new RespuestaApi<bool>(resultado);

         return Ok(respuesta);
      }

      /// <summary>
      /// Elimina una tienda a partir de un Id.
      /// </summary>
      /// <param name="id">Identificador de la tienda.</param>
      /// <returns>true/false</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<bool>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpDelete("{id}", Name = nameof(EliminarTienda))]
      public async Task<IActionResult> EliminarTienda(int id)
      {
         var resultado = await _tiendaServicio.EliminarTienda(id);
         var respuesta = new RespuestaApi<bool>(resultado);

         return Ok(respuesta);
      }
   }
}
