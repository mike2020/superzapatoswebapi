﻿using AutoMapper;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._04Dtos.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._05Excepciones;
using Elipgo.SuperZapatos.WebApi.Core._06Servicios.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using Elipgo.SuperZapatos.WebApi.Infraestructura._07Intefaces;
using Elipgo.SuperZapatos.WebApi.Respuestas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Controllers.Catalogos
{
   [Authorize]
   [Produces("application/json")]
   [Route("api/[controller]")]
   [ApiController]
   public class ArticulosController : ControllerBase
   {
      private readonly IArticuloServicio _articuloServicio;
      private readonly IMapper _mapeador;
      private readonly IUriServicio _uriServicio;

      public ArticulosController(
                                 IArticuloServicio articuloServicio,
                                 IMapper mapeador,
                                 IUriServicio uriServicio
                                )
      {
         _articuloServicio = articuloServicio;
         _mapeador = mapeador;
         _uriServicio = uriServicio;
      }

      /// <summary>
      /// Obtiene todos los articulos.
      /// </summary>
      /// <param name="filtros">Filtros por aplicar.</param>
      /// <returns>Listado de articulos</returns>
      /// <remarks>Este método obtiene el listado de los artículos por página.</remarks>
      /// <response code="200">La solicitud ha tenido éxito.</response>
      ///////////////// <response code="201">Se ha creado un nuevo recurso.</response>
      /// <response code="400">El servidor no puedo interpretar la solicitud.</response>
      /// <response code="401">No esta autorizado, es necesario autenticarse.</response>
      /// <response code="403">No se tienen permisos.</response>
      /// <response code="404">Recurso no encontrado.</response>
      /// <response code="500">Error interno del servidor.</response>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<IEnumerable<ArticuloDto>>))]
      //[ProducesResponseType((int)HttpStatusCode.Created, Type = typeof(ExcepcionNegocio))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(RespuestaApi<IEnumerable<ArticuloDto>>))]
      [ProducesResponseType((int)HttpStatusCode.Unauthorized, Type = typeof(RespuestaApi<IEnumerable<ArticuloDto>>))]
      [ProducesResponseType((int)HttpStatusCode.NotFound, Type = typeof(RespuestaApi<IEnumerable<ArticuloDto>>))]
      [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(RespuestaApi<IEnumerable<ArticuloDto>>))]
      [HttpGet(Name = nameof(ObtenerArticulos))]
      public IActionResult ObtenerArticulos([FromQuery]ArticuloFiltrosConsulta filtros) 
      {
         var articulos = _articuloServicio.ObtenerArticulos(filtros);
         var articulosDto = _mapeador.Map<IEnumerable<ArticuloDto>>(articulos);

         /*---   Se muestra en la respuesta la información sobre la paginación   ---*/
         var metadata = new Metadata
         {
            TotalRenglones = articulos.TotalRenglones,
            TamanioPagina = articulos.TamanioPagina,
            PaginaActual = articulos.PaginaActual,
            TotalPaginas = articulos.TotalPaginas,
            TienePaginaSiguiente = articulos.TienePaginaSiguiente,
            TienePaginaAnterior = articulos.TienePaginaAnterior,
            UrlPaginaSiguiente = _uriServicio.ObtenerArticuloPaginacionUri(filtros, Url.RouteUrl(nameof(ObtenerArticulos))).ToString(),
            UrlPaginaAnterior = _uriServicio.ObtenerArticuloPaginacionUri(filtros, Url.RouteUrl(nameof(ObtenerArticulos))).ToString()
         };
         //Response.Headers.Add("X-Paginacion", JsonConvert.SerializeObject(metadata));

         var respuesta = new RespuestaApi<IEnumerable<ArticuloDto>>(articulosDto)
         {
            Meta = metadata
         };

         return Ok(respuesta);
      }

      /// <summary>
      /// Obtiene un artículo a partir del Id.
      /// </summary>
      /// <param name="id">Identificador del artículo</param>
      /// <returns>Artículo</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<ArticuloDto>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpGet("{id}", Name = nameof(ObtenerArticulo))]
      public async Task<IActionResult> ObtenerArticulo(int id)
      {
         var articulo = await _articuloServicio.ObtenerArticulo(id);
         var articuloDto = _mapeador.Map<ArticuloDto>(articulo);

         var respuesta = new RespuestaApi<ArticuloDto>(articuloDto);

         return Ok(respuesta);
      }

      /// <summary>
      /// Agrega un artículo.
      /// </summary>
      /// <param name="articuloDto">Artículo</param>
      /// <returns></returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<ArticuloDto>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpPost(Name = nameof(AgregarArticulo))]
      public async Task<IActionResult> AgregarArticulo(ArticuloDto articuloDto)
      {
         var articulo = _mapeador.Map<Article>(articuloDto);

         await _articuloServicio.AgregarArticulo(articulo);

         articuloDto = _mapeador.Map<ArticuloDto>(articulo);

         var respuesta = new RespuestaApi<ArticuloDto>(articuloDto);

         return Ok(respuesta);
      }

      /// <summary>
      /// Actualiza la información del artículo.
      /// </summary>
      /// <param name="id">Identificador del artículo.</param>
      /// <param name="articuloDto">Artículo</param>
      /// <returns>true/false</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<bool>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpPut("{id}", Name = nameof(ActualizarArticulo))]
      public async Task<IActionResult> ActualizarArticulo(int id, ArticuloDto articuloDto)
      {
         var articulo = _mapeador.Map<Article>(articuloDto);
         articulo.Id = id;

         var resultado = await _articuloServicio.ActualizarArticulo(articulo);
         var respuesta = new RespuestaApi<bool>(resultado);

         return Ok(respuesta);
      }

      /// <summary>
      /// Elimina un artículo a partir de un Id.
      /// </summary>
      /// <param name="id">Identificador del artículo.</param>
      /// <returns>trye/false</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<bool>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpDelete("{id}", Name = nameof(EliminarArticulo))]
      public async Task<IActionResult> EliminarArticulo(int id)
      {
         var resultado = await _articuloServicio.EliminarArticulo(id);
         var respuesta = new RespuestaApi<bool>(resultado);

         return Ok(respuesta);
      }
   }
}
