﻿using AutoMapper;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._04Dtos.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._06Servicios.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using Elipgo.SuperZapatos.WebApi.Infraestructura._07Intefaces;
using Elipgo.SuperZapatos.WebApi.Respuestas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Controllers.Modulos
{
   [Authorize]
   [Produces("application/json")]
   [Route("api/[controller]")]
   [ApiController]
   public class PreciosController : ControllerBase
   {
      private readonly IPrecioServicio _precioServicio;
      private readonly IMapper _mapeador;
      private readonly IUriServicio _uriServicio;

      public PreciosController(
                               IPrecioServicio precioServicio,
                               IMapper mapeador,
                               IUriServicio uriServicio
                              )
      {
         _precioServicio = precioServicio;
         _mapeador = mapeador;
         _uriServicio = uriServicio;
      }

      /// <summary>
      /// Obtiene tdos los precios.
      /// </summary>
      /// <param name="filtros">Filtros por aplicar</param>
      /// <returns>Listado de precios.</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<IEnumerable<PrecioDto>>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpGet(Name = nameof(ObtenerPrecios))]
      public IActionResult ObtenerPrecios([FromQuery] PrecioFiltrosConsulta filtros)
      {
         var precios = _precioServicio.ObtenerPrecios(filtros);
         var preciosDto = _mapeador.Map<IEnumerable<PrecioDto>>(precios);

         /*---   Se muestra en la respuesta la información sobre la paginación   ---*/
         var metadata = new Metadata
         {
            TotalRenglones = precios.TotalRenglones,
            TamanioPagina = precios.TamanioPagina,
            PaginaActual = precios.PaginaActual,
            TotalPaginas = precios.TotalPaginas,
            TienePaginaSiguiente = precios.TienePaginaSiguiente,
            TienePaginaAnterior = precios.TienePaginaAnterior,
            UrlPaginaSiguiente = _uriServicio.ObtenerPrecioPaginacionUri(filtros, Url.RouteUrl(nameof(ObtenerPrecios))).ToString(),
            UrlPaginaAnterior = _uriServicio.ObtenerPrecioPaginacionUri(filtros, Url.RouteUrl(nameof(ObtenerPrecios))).ToString()
         };

         var respuesta = new RespuestaApi<IEnumerable<PrecioDto>>(preciosDto);

         return Ok(respuesta);
      }

      /// <summary>
      /// Obtiene un precio a partir del Id.
      /// </summary>
      /// <param name="id">Identificador del precio</param>
      /// <returns>Precio</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<PrecioDto>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpGet("{id}", Name = nameof(ObtenerPrecio))]
      public async Task<IActionResult> ObtenerPrecio(int id)
      {
         var precio = await _precioServicio.ObtenerPrecio(id);
         var precioDto = _mapeador.Map<PrecioDto>(precio);

         var respuesta = new RespuestaApi<PrecioDto>(precioDto);

         return Ok(respuesta);
      }

      /// <summary>
      /// Agrega un precio.
      /// </summary>
      /// <param name="precioDto">Precio</param>
      /// <returns></returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<PrecioDto>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpPost(Name = nameof(AgregarPrecio))]
      public IActionResult AgregarPrecio(PrecioDto precioDto)
      {
         var precio = _mapeador.Map<Price>(precioDto);

         _precioServicio.AgregarPrecio(precio);

         precioDto = _mapeador.Map<PrecioDto>(precio);

         var respuesta = new RespuestaApi<PrecioDto>(precioDto);

         return Ok(respuesta);
      }

      /// <summary>
      /// Actualiza la información del precio.
      /// </summary>
      /// <param name="id">Identificador del precio.</param>
      /// <param name="precioDto">Precio</param>
      /// <returns>true/false</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<bool>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpPut("{id}", Name = nameof(ActualizarPrecio))]
      public async Task<IActionResult> ActualizarPrecio(int id, PrecioDto precioDto)
      {
         var precio = _mapeador.Map<Price>(precioDto);
         precio.Id = id;

         var resultado = await _precioServicio.ActualizarPrecio(precio);
         var respuesta = new RespuestaApi<bool>(resultado);

         return Ok(respuesta);
      }

      /// <summary>
      /// Elimina un precio a partir de un Id.
      /// </summary>
      /// <param name="id">Identificador del precio.</param>
      /// <returns>true/false</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<bool>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpDelete("{id}", Name = nameof(EliminarPrecio))]
      public async Task<IActionResult> EliminarPrecio(int id)
      {
         var resultado = await _precioServicio.EliminarPrecio(id);
         var respuesta = new RespuestaApi<bool>(resultado);

         return Ok(respuesta);
      }
   }
}
