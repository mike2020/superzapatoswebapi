﻿using AutoMapper;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._04Dtos.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._06Servicios.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;
using Elipgo.SuperZapatos.WebApi.Infraestructura._07Intefaces;
using Elipgo.SuperZapatos.WebApi.Respuestas;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Controllers.Modulos
{
   [Authorize]
   [Produces("application/json")]
   [Route("api/[controller]")]
   [ApiController]
   public class InventariosController : ControllerBase
   {
      private readonly IInventarioServicio _inventarioServicio;
      private readonly IMapper _mapeador;
      private readonly IUriServicio _uriServicio;

      public InventariosController(
                                   IInventarioServicio inventarioServicio,
                                   IMapper mapeador,
                                   IUriServicio uriServicio
                                  )
      {
         _inventarioServicio = inventarioServicio;
         _mapeador = mapeador;
         _uriServicio = uriServicio;
      }

      /// <summary>
      /// Obtiene todos los inventarios.
      /// </summary>
      /// <param name="filtros">Filtros por aplicar</param>
      /// <returns>Listado de inventarios</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<IEnumerable<InventarioDto>>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpGet(Name = nameof(ObtenerInventarios))]
      public IActionResult ObtenerInventarios([FromQuery] InventarioFiltrosConsulta filtros)
      {
         var inventarios = _inventarioServicio.ObtenerInventarios(filtros);
         var inventariosDto = _mapeador.Map<IEnumerable<InventarioDto>>(inventarios);

         /*---   Se muestra en la respuesta la información sobre la paginación   ---*/
         var metadata = new Metadata
         {
            TotalRenglones = inventarios.TotalRenglones,
            TamanioPagina = inventarios.TamanioPagina,
            PaginaActual = inventarios.PaginaActual,
            TotalPaginas = inventarios.TotalPaginas,
            TienePaginaSiguiente = inventarios.TienePaginaSiguiente,
            TienePaginaAnterior = inventarios.TienePaginaAnterior,
            UrlPaginaSiguiente = _uriServicio.ObtenerInventarioPaginacionUri(filtros, Url.RouteUrl(nameof(ObtenerInventarios))).ToString(),
            UrlPaginaAnterior = _uriServicio.ObtenerInventarioPaginacionUri(filtros, Url.RouteUrl(nameof(ObtenerInventarios))).ToString()
         };

         var respuesta = new RespuestaApi<IEnumerable<InventarioDto>>(inventariosDto)
         {
            Meta = metadata
         };

         return Ok(respuesta);
      }

      /// <summary>
      /// Obtiene un inventario a partir del Id.
      /// </summary>
      /// <param name="id">Identificador del inventario</param>
      /// <returns>Inventario</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<InventarioDto>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpGet("{id}", Name = nameof(ObtenerInventario))]
      public async Task<IActionResult> ObtenerInventario(int id)
      {
         var inventario = await _inventarioServicio.ObtenerInventario(id);
         var inventarioDto = _mapeador.Map<InventarioDto>(inventario);

         var respuesta = new RespuestaApi<InventarioDto>(inventarioDto);

         return Ok(respuesta);
      }

      /// <summary>
      /// Agrega un inventario.
      /// </summary>
      /// <param name="inventarioDto">Inventario</param>
      /// <returns></returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<InventarioDto>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpPost(Name = nameof(AgregarInventario))]
      public IActionResult AgregarInventario(InventarioDto inventarioDto)
      {
         var inventario = _mapeador.Map<Inventory>(inventarioDto);

         _inventarioServicio.AgregarInventario(inventario);

         inventarioDto = _mapeador.Map<InventarioDto>(inventario);

         var respuesta = new RespuestaApi<InventarioDto>(inventarioDto);

         return Ok(respuesta);
      }

      /// <summary>
      /// Actualiza la información del inventaro.
      /// </summary>
      /// <param name="id">Identificador del inventario.</param>
      /// <param name="inventarioDto">Inventario</param>
      /// <returns>true/false</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<bool>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpPut("{id}", Name = nameof(ActualizarInventario))]
      public async Task<IActionResult> ActualizarInventario(int id, InventarioDto inventarioDto)
      {
         var inventario = _mapeador.Map<Inventory>(inventarioDto);
         inventario.Id = id;

         var resultado = await _inventarioServicio.ActualizarInventario(inventario);
         var respuesta = new RespuestaApi<bool>(resultado);

         return Ok(respuesta);
      }

      /// <summary>
      /// Elimina un inventario a partir de un Id.
      /// </summary>
      /// <param name="id">Identificador del inventario.</param>
      /// <returns>true/false</returns>
      [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(RespuestaApi<bool>))]
      [ProducesResponseType((int)HttpStatusCode.BadRequest)]
      [ProducesResponseType((int)HttpStatusCode.NotFound)]
      [HttpDelete("{id}", Name = nameof(EliminarInventario))]
      public async Task<IActionResult> EliminarInventario(int id)
      {
         var resultado = await _inventarioServicio.EliminarInventario(id);
         var respuesta = new RespuestaApi<bool>(resultado);

         return Ok(respuesta);
      }
   }
}
