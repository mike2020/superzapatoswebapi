﻿using AutoMapper;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._04Dtos.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._06Servicios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Respuestas;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Controllers.Seguridad
{
   [Produces("application/json")]
   [Route("api/[controller]")]
   [ApiController]
   public class RolesApiController : ControllerBase
   {
      public readonly  IRolApiServicio _rolApiServicio;
      private readonly IMapper _mapeador;

      public RolesApiController(
                                IRolApiServicio rolApiServicio,
                                IMapper mapeador
                               )
      {
         _rolApiServicio = rolApiServicio;
         _mapeador = mapeador;
      }

      [HttpPost]
      public async Task<IActionResult> AgregarRolesApi(RolApiDto rolApiDto)
      {
         var rol = _mapeador.Map<RoleApi>(rolApiDto);

         await _rolApiServicio.Agregar(rol);

         rolApiDto = _mapeador.Map<RolApiDto>(rol);

         var respuesta = new RespuestaApi<RolApiDto>(rolApiDto);

         return Ok(respuesta);
      }
   }
}
