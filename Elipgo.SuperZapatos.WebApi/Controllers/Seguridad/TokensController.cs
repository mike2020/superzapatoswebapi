﻿//using AutoMapper.Configuration;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._05Excepciones;
using Elipgo.SuperZapatos.WebApi.Core._06Servicios.Seguridad;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Controllers.Seguridad
{
   [Route("api/[controller]")]
   [ApiController]
   public class TokensController : ControllerBase
   {
      public readonly IConfiguration _configuracion;
      public readonly IUsuarioApiServicio _usuarioApiSServicio;
      public readonly IRolApiServicio _rolApiServicio;
      //public readonly ITokenServicio _tokenServicio;

      public TokensController(
                             IConfiguration configuracion,
                             IUsuarioApiServicio usuarioApiSServicio,
                             IRolApiServicio rolApiServicio
                            //ITokenServicio tokenServicio
                            )
      {
         _configuracion = configuracion;
         _usuarioApiSServicio = usuarioApiSServicio;
         _rolApiServicio = rolApiServicio;
         //_tokenServicio = tokenServicio;
      }

      [HttpPost]
      public async Task<IActionResult> Autenticacion(InicioSesion inicioSesion)
      {
         var validacion = await ValidarUsuario(inicioSesion);
         if (validacion.Item1)
         {
            var token = await GenerarToken(validacion.Item2);

            return Ok(new { token });
         }

         return NotFound();
      }

      private async Task<(bool, UserApi)> ValidarUsuario(InicioSesion inicioSesion)
      {
         var usuario = await _usuarioApiSServicio.ObtenerCredenciaes(inicioSesion);

         return (usuario != null, usuario);
      }

      private async Task<string> GenerarToken(UserApi usuario)
      {
         //var token = _tokenServicio.GenerarToken(inicioSesion).ToString();

         //Header
         var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuracion["Authentication:SecretKey"]));
         var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
         var header = new JwtHeader(signingCredentials);

         var rol = await _rolApiServicio.ObtenerPorId(usuario.RoleApiId);

         if (rol == null) {
            throw new ExcepcionNegocio("El usuario no tiene un rol conocido");
         }

         //Cleamis
         var claims = new[]
         {
            new Claim(ClaimTypes.Name, (usuario.NameUser + " " + usuario.LastNameUser + " " + usuario.MothersLastNameUser).ToString()),
            new Claim(ClaimTypes.Email, usuario.EmailUser),
            new Claim(ClaimTypes.Role, rol.Name)
         };

         //Payload
         var payload = new JwtPayload
         (
            _configuracion["Authentication:Issuer"],
            _configuracion["Authentication:Audience"],
            claims,
            DateTime.Now,
            DateTime.UtcNow.AddMinutes(200)
         );

         var token = new JwtSecurityToken(header, payload);

         return new JwtSecurityTokenHandler().WriteToken(token);
      }
   }
}
