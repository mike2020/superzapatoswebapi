﻿using AutoMapper;
using Elipgo.SuperZapatos.WebApi.Core._01Entidades.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._04Dtos.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._06Servicios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Respuestas;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Elipgo.SuperZapatos.WebApi.Controllers.Seguridad
{
   [Produces("application/json")]
   [Route("api/[controller]")]
   [ApiController]
   public class UsuariosApiController : ControllerBase
   {
      public readonly IUsuarioApiServicio _usuarioApiServicio;
      private readonly IMapper _mapeador;

      public UsuariosApiController(
                                   IUsuarioApiServicio usuarioApiServicio,
                                   IMapper mapeador
                                   )
      {
         _usuarioApiServicio = usuarioApiServicio;
         _mapeador = mapeador;
      }

      [HttpPost]
      public async Task<IActionResult> AgregarUsuarioApi(UsuarioApiDto usuarioApiDto)
      {
         var usuario = _mapeador.Map<UserApi>(usuarioApiDto);

         await _usuarioApiServicio.Agregar(usuario);

         usuarioApiDto = _mapeador.Map<UsuarioApiDto>(usuario);

         var respuesta = new RespuestaApi<UsuarioApiDto>(usuarioApiDto);

         return Ok(respuesta);
      }
   }
}
