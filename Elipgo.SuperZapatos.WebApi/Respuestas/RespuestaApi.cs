﻿using Elipgo.SuperZapatos.WebApi.Core._07FiltrosConsulta.Paginacion;

namespace Elipgo.SuperZapatos.WebApi.Respuestas
{
   public class RespuestaApi<T>
   {
      public RespuestaApi(T datos)
      {
         Datos = datos;
      }
      public T Datos { get; set; }
      public Metadata Meta { get; set; }
   }
}
