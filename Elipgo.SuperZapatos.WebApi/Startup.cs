using AutoMapper;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._02Interfaces.Repositorios.Seguridad._Base;
using Elipgo.SuperZapatos.WebApi.Core._06Servicios.Catalogos;
using Elipgo.SuperZapatos.WebApi.Core._06Servicios.Modulos;
using Elipgo.SuperZapatos.WebApi.Core._06Servicios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Core._08Opciones;
using Elipgo.SuperZapatos.WebApi.Infraestructura._01Datos.Contextos;
using Elipgo.SuperZapatos.WebApi.Infraestructura._02Repositorios;
using Elipgo.SuperZapatos.WebApi.Infraestructura._02Repositorios.Seguridad;
using Elipgo.SuperZapatos.WebApi.Infraestructura._02Repositorios.Seguridad._Base;
using Elipgo.SuperZapatos.WebApi.Infraestructura._03Filtros;
using Elipgo.SuperZapatos.WebApi.Infraestructura._06Servicios;
using Elipgo.SuperZapatos.WebApi.Infraestructura._07Intefaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
//using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace Elipgo.SuperZapatos.WebApi
{
   public class Startup
   {
      private readonly string MisOriginesCors = "_misOrigenesCors";
      public Startup(IConfiguration configuration)
      {
         Configuration = configuration;
      }

      public IConfiguration Configuration { get; }

      // This method gets called by the runtime. Use this method to add services to the container.
      public void ConfigureServices(IServiceCollection services)
      {
         /*---   Hace posible el mapeo de objetos para exponer el DTO en lugar de la Entidad de BD   ---*/
         services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

         services.Configure<PaginacionOpciones>(Configuration.GetSection("Pagination"));
         //services.Configure<AutenticacionOpciones>(Configuration.GetSection("Authentication"));

         /*---   Se agrega el Contexto y se hace posible la inyección de dependencia de este objeto   ---*/
         services.AddDbContext<ContextoDev>(options =>
            /*---   Se indica que se usará SQL Server para el acceso a datos   ---*/
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
         //////services.AddDbContext<ContextoDev>(options =>
         //////             options.UseSqlServer(
         //////                 Configuration.GetConnectionString("DefaultConnection")));
         //////services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
         //////    .AddEntityFrameworkStores<Contexto>();

         //////services.AddIdentity<ApplicationUser, IdentityRole>()
         //////        .AddEntityFrameworkStores<ContextoDev>()
         //////        .AddDefaultTokenProviders();

         services.AddControllers(options =>
         {
            options.Filters.Add<FitroExcepcionGlobal>();
         }).AddNewtonsoftJson(options =>
            {
               /*---   Con esta instrucción se evita el error de la referencia circular causada por las propiedades de navegación ---*/
               options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
               options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            })
            .ConfigureApiBehaviorOptions(options =>
            {
               /*---   Con esta instrucción se suprime la validación del modelo manual y lo controla .Net Core   ---*/
               //options.SuppressModelStateInvalidFilter = true;
            });

         services.AddCors(options =>
         {
            options.AddPolicy(name: MisOriginesCors,
                              builder =>
                              {
                                 builder.WithOrigins("https://localhost/SuperZapatos/swagger",
                                                     "https://localhost/SuperZapatos/")
                                        .AllowAnyHeader()
                                        .WithMethods("GET", "POST", "PUT", "DELETE");
                              });
         });

         ////////services.Configure<IdentityOptions>(options =>
         ////////{
         ////////   // Password settings.
         ////////   options.Password.RequireDigit = true;
         ////////   options.Password.RequireLowercase = true;
         ////////   options.Password.RequireNonAlphanumeric = true;
         ////////   options.Password.RequireUppercase = true;
         ////////   options.Password.RequiredLength = 6;
         ////////   options.Password.RequiredUniqueChars = 1;

         ////////   // Lockout settings.
         ////////   options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
         ////////   options.Lockout.MaxFailedAccessAttempts = 5;
         ////////   options.Lockout.AllowedForNewUsers = true;

         ////////   // User settings.
         ////////   options.User.AllowedUserNameCharacters =
         ////////   "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
         ////////   options.User.RequireUniqueEmail = false;
         ////////});

         //////services.ConfigureApplicationCookie(options =>
         //////{
         //////   // Cookie settings
         //////   options.Cookie.HttpOnly = true;
         //////   options.ExpireTimeSpan = TimeSpan.FromMinutes(5);

         //////   options.LoginPath = "/Identity/Account/Login";
         //////   options.AccessDeniedPath = "/Identity/Account/AccessDenied";
         //////   options.SlidingExpiration = true;
         //////});

         /*---   Inyección de dependencias   ---*/
         services.AddTransient<IArticuloServicio, ArticuloServicio>();
         services.AddTransient<ITiendaServicio, TiendaServicio>();
         services.AddTransient<IInventarioServicio, InventarioServicio>();
         services.AddTransient<IPrecioServicio, PrecioServicio>();

         services.AddTransient<IUsuarioApiServicio, UsuarioApiServicio>();
         services.AddTransient<IRolApiServicio, RolApiServicio>();

         //services.AddTransient<ITokenServicio, TokenServicio>();

         services.AddScoped(typeof(IRepositorio<>), typeof(Repositorio<>));
         services.AddScoped(typeof(IBaseRepositorioSeguridad<>), typeof(BaseRepositorioSeguridad<>));

         services.AddScoped(typeof(IUsuarioApiRepositorioSeguridad), typeof(UsuarioApiRepositorioSeguridad));
         services.AddScoped(typeof(IRolApiRepositorioSeguridad), typeof(RolApiRepositorioSeguridad));

         //services.AddScoped(typeof(ITokenRepositorio), typeof(TokenRepositorio));

         services.AddTransient<IUnidadTrabajo, UnidadTrabajo>();
         services.AddTransient<IUnidadTrabajoSeguridad, UnidadTrabajoSeguridad>();

         services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
         services.AddSingleton<IUriServicio>(proveedor =>
         {
            var accesor = proveedor.GetRequiredService<IHttpContextAccessor>();
            var peticion = accesor.HttpContext.Request;
            var uriAbsoluta = String.Concat(peticion.Scheme, "://", peticion.Host.ToUriComponent());
            return new UriServicio(uriAbsoluta);
         });

         services.AddSwaggerGen(documents =>
         {
            documents.SwaggerDoc("v1", new OpenApiInfo { Title = "Super Zapatos API", Version = "v1" });

            var archivXML = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var rutaXML = Path.Combine(AppContext.BaseDirectory, archivXML);

            documents.IncludeXmlComments(rutaXML);
         });

         services.AddAuthentication(options =>
         {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
         }).AddJwtBearer(options =>
         {
            options.TokenValidationParameters = new TokenValidationParameters
            {
               ValidateIssuer = true,
               ValidateAudience = true,
               ValidateLifetime = true,
               ValidateIssuerSigningKey = true,
               ValidIssuer = Configuration["Authentication:Issuer"],
               ValidAudience = Configuration["Authentication:Audience"],
               IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Authentication:SecretKey"]))
            };
         });

         services.AddMvcCore(options =>
         {
            /*---   Se implementa la validación del modelo a nivel app   ---*/
            //options.Filters.Add<FiltroValidacionModelo>();
         });
         //////.AddFluentValidation(options => 
         //////{
         //////   /*---   Se habilita la configuración de la validación de Entidades (Fluent Validation)   ---*/
         //////   options.RegisterValidatorsFromAssemblies(AppDomain.CurrentDomain.GetAssemblies());
         //////});
      }

      // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
      public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
      {
         if (env.IsDevelopment())
         {
            app.UseDeveloperExceptionPage();
         }

         app.UseHttpsRedirection();

         app.UseSwagger();
         app.UseSwaggerUI(options =>
         {
            options.SwaggerEndpoint("../swagger/v1/swagger.json", "Super Zapatos API v1");
            //options.RoutePrefix = "swagger"; // String.Empty;
         });

         app.UseCors(MisOriginesCors);

         app.UseRouting();

         app.UseAuthentication();

         app.UseAuthorization();

         app.UseEndpoints(endpoints =>
         {
            endpoints.MapControllers();
         });
      }
   }
}
